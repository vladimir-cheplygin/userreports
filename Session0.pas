unit Session0;
interface
uses
  IdGlobal,
  String0,
  Tim0,
  Email0,
  LynxErrorReports;

type
  Stats__ = class
    Junks   :cardinal;
    Reports :cardinal;
    Others  :cardinal;
    Copied  :cardinal;
    procedure Clear;
  end;

  MailSession__ = class (TObject)
   private
     _Host   :string;
     _Port   :TIdPort;
   private
     _Pass   :string;
     _User   :string;
     _Recent :boolean;
     function get_UserName :string;
   private
    _Server :MailServer__;
    _Recoverable :boolean;
    function get_Server: MailServer__;
   private
    function NewMailServer :MailServer__;
    procedure GetJunk (Junk :Str__);
   private
    _Stats        :Stats__;
    _ReportDelete :boolean;
    _JunkDelete   :boolean;
    _Verbose      :boolean;
   private
    _TargetRoot   :string;
    procedure set_TargetRoot (const aValue :string);
   private
    _DB :TLynxErrorReports;
   protected
     property Server :MailServer__ read get_Server;
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    property Host :string write _Host;
    property Port :TIdPort write _Port;
    property User :string write _User;
    property Pass :string write _Pass;
    property Recent :boolean write _Recent;
    property JunkDelete :boolean write _JunkDelete;
    property ReportDelete :boolean write _ReportDelete;
    property Verbose :boolean write _Verbose;
    property TargetRoot :string write set_TargetRoot;

    function IsConnected: boolean;
    property Recoverable :boolean read _Recoverable;
    function Process :boolean;
    property Stats :Stats__ read _Stats;
    end;

implementation
uses
  SysUtils,
  ///Log2,
  ///List2,
  IdStack;

  procedure Stats__.Clear;
  begin
    Junks := 0;
    Reports := 0;
    Others := 0;
    Copied := 0;
  end;




  constructor MailSession__.Create;
  begin
    inherited Create;
    _Stats := Stats__.Create;
    _Db := TLynxErrorReports.Create(nil);
  end;

  destructor MailSession__.Destroy;
  begin
    _Stats.Free;
    _Db.Connected := False;
    _Db.Free;
    inherited Destroy;
  end;

  function MailSession__.get_UserName: string;
  begin
    if _Recent then
      Result := 'recent:' + _User
    else
      Result := _User;
  end;

  procedure MailSession__.GetJunk (Junk :Str__);
  const
    s :array[0..9] of string = (
      'AutoScheduleProjects succeeded',
      'AutoScheduleProjects completed',
      'Logfile succeeded scheduled report',
      'Exception on Localserver',
      'Exception in AutoScheduleProjects',
      'Exception on Azure server',
      'Error scheduled reports',
      'Error occurred in notifications',
      'TestDocumentAccessRights failure',
      'Your report ('
      );
  var
    i    :integer;
  begin
    Junk.Clear;
    for i := Low (s) to High (s) do
      Junk.Add (s[i]);
  end;

  procedure MailSession__.set_TargetRoot (const aValue: string);
  begin
    _TargetRoot := aValue;
  end;

  function MailSession__.NewMailServer :MailServer__;
  begin
    Result := MailServer__.Create(nil);
    Result.Host := _Host;
    Result.BoundPort := _Port;
    Result.Username := get_UserName;
    Result.Password := _Pass;
    GetJunk(Result.JunkSubjects);
  end;

  function MailSession__.get_Server: MailServer__;
  begin
    if (_Server = nil) then
      _Server := NewMailServer;
    Result := _Server;
  end;

  function MailSession__.IsConnected :boolean;
  begin
    Result := false;
    _Recoverable := false;
    if Server.Connected then
      Result := true
    else
      try
        Server.Connect;
        if Server.Connected then
          Result := true;
      except
        on e :EIdSocketError do begin
          if (EIdSocketError(E).LastError = 11001) then begin       //host not found = recoverable (in sleep now)
            Result := false;
            _Recoverable := true;
            end
          else if (EIdSocketError(E).LastError = 10060) then begin  //connection timed out
            Result := false;
            _Recoverable := true;
            end;
          end;
        on e :Exception do
          Result := false;
      end;
  end;

  function MailSession__.Process: boolean;
  const
    RecentStr :array[boolean] of string = ('new', 'all');
  var
    Msg  :Email__;
    i    :integer;
  begin
    Result := false;
    _Stats.Clear;
    try
      if IsConnected then
      if _Db.Connect then
      try
        Result := true;
        for i := 0 to Pred(Server.Count) do begin
          Msg :=  Server.GetEmail (i);
          if Msg.IsIrrelevant then begin
            Inc (_Stats.Junks);
            if _Verbose then
              Msg.DoLog ('server:');
            if _JunkDelete then
              Msg.Delete;
            end
          else if not Msg.IsBugReport then begin
            Inc (_Stats.Others);
            if _Verbose then
              Msg.DoLog ('other: ');
            end
          else if (Msg.Report <> nil) then begin
            Inc (_Stats.Reports);
            if _Db.Import(Msg.Report.Text, Msg.Report.Image, Msg.Date) then begin
              Msg.DoLog ('report:');
              Inc (_Stats.Copied);
              end;
            if _ReportDelete then
              Msg.Delete;
            end
          else
            Msg.DoLog ('error:');
          end;
      except
        on e :Exception do begin
          ///Logger.Exception (Self, e);
          WriteLn (e.Message);
          Result := False;
          end;
        end;
      WriteLn (Format('%s %s emails/server/other/reports: %d/%d/%d/%d', [FormatDateTime('yyyy-mm-dd hh:mm', Now), RecentStr[_Recent], _Server.Count, _Stats.Junks, _Stats.Others, _Stats.Reports]));
    finally
      FreeAndNil (_Server);
      end;
  end;

initialization
finalization
end.