unit A0;
interface
uses
  SysUtils;
var
  StdSettings :TFormatSettings;
type
  Decimals_ = 0..20;
  FloatValue__ = class (TObject)
   private
    _Decided :boolean;
   protected
   public
    constructor Create;
    destructor Destroy;
      override;
    class function DecimalPower(aDecimals :Decimals_) :double;
    class function RoundTo (aValue :double; aDecimals :Decimals_) :double;
    function FromString (aValue :string) :double;
    end;


  function Get7Bit (aValue :array of byte) :integer;
  function IntToStr2 (aValue :integer) :string;
  function StrToInt (aValue :string) :integer;
  procedure SWP (var Int1, Int2 :integer);
  procedure SwapBytes (var Int1, Int2 :byte);
  procedure PRE (var Cur :integer; Low, High :integer);
  procedure SUC (var Cur :integer; Low, High :integer);
  procedure LOWER (var What :integer; Limit :integer);
  procedure RAIS (var What :integer; Limit :integer);
  procedure CLIP (var What :integer; Low, High :integer);
  function OCCUPY (Bytes, Size :integer) :integer;
  function Modulo (X, N :integer) :integer;
  function CEN (Size, MaxSize :integer) :integer;
  function BTW (Center, Left, Right :integer) :boolean;
  function WID (aValue :integer) :ShortInt;

const
  Binary = 2;
  Decimal = 10;
  Hexadecimal = 16;
  NumStrMax = 16;
  DecChars = ['0'..'9'];
  DecCharsStr = '0123456789';
  HexChars = DecChars + ['A'..'F'];
  HexCharsStr = 'ABCDEF';
type
  Base_ = 2..Hexadecimal;
  NumStrIdx_ = 1..NumStrMax;
  NumStr_ = string[NumStrMax];
var
  CurrentNumericBase :Base_ = Decimal;
  Digits      :NumStr_ = '';
  LeadingSign :Char = '0';
  Digit       :array [0..PRED (Hexadecimal)] of Char = DecCharsStr+HexCharsStr;
//const

  procedure SET_BASE (NewBase :Base_);
  function LIM (Width :integer) :integer;

  function DIG (Ch :Char) :boolean;
  function CommaSeparated (const aValue :string) :string;
  function PercentOf (aValue, aPct :word) :word;
    overload;
  function PercentOf (aValue, aPct :integer) :integer;
    overload;
  function PercentOf (aValue, aPct :double) :integer;
    overload;
  function PercentOf (aValue, aPct :cardinal) :cardinal;
    overload;
  function Percent (a, b :double) :double;
    overload;
  function PercentStr (a, b :double) :string;
  function Ratio (a, b :double) :double;
  //function PctStr (aDiff, aBase :integer) :string;
  function FloatStr (aValue :double) :string;
  function Whole (aValue :double) :boolean;
  procedure Floating (aValue :boolean);
  function FloatToStr (aValue :double) :string;
  function StrToFloatDef (aValue :string; aDef :double) :double;
  function MoneyToStr (aValue :integer) :string;
  function StrToMoney (aValue :string) :integer;

var
  FloatValue :FloatValue__;

implementation
uses
  Windows,
  Math,
  Char0,
  String0;

  function Get7Bit (aValue :array of byte) :integer;
  const
    Bits = 7;
  var
    i :ShortInt;
  begin
    Assert ((High (aValue)-Low(aValue)) < SizeOf (Result));
    Result := 0;
    for i := High (aValue) downto Low (aValue) do
      Result := Result + (aValue[i] SHL (Bits * (High (aValue) - i)));
  end;


  function Whole (aValue :double) :boolean;
  const
    Delta = 1e-6;
  begin
    Result := (Abs (Frac (aValue)) < Delta);
  end;

  procedure SWP (var Int1, Int2 :integer);
  var
    Save :integer;
  begin
    Save := Int1;
    Int1 := Int2;
    Int2 := Save;
  end;

  procedure SwapBytes (var Int1, Int2 :byte);
  var
    Save :byte;
  begin
    Save := Int1;
    Int1 := Int2;
    Int2 := Save;
  end;

  procedure PRE (var Cur :integer; Low, High :integer);
  begin
    if (Cur <= Low) then Cur := High else Cur := PRED (Cur);
  end;

  procedure SUC (var Cur :integer; Low, High :integer);
  begin
    if (Cur >= High) then Cur := Low else Cur := SUCC (Cur);
  end;

  procedure LOWER (var What :integer; Limit :integer);
  begin
    if (What > Limit) then What := Limit;
  end;

  procedure RAIS (var What :integer; Limit :integer);
  begin
    if (What < Limit) then What := Limit;
  end;

  procedure CLIP (var What :integer; Low, High :integer);
  begin
    if (What < Low) then What := Low
    else if (What > High) then What := High;
    {RAIS(what, low); lower (what, high)}
  end;

  function OCCUPY (Bytes, Size :integer) :integer;
  begin
    Result := 0;
    if (Size > 0) then begin
      Result := (Bytes div Size);
      if ((Bytes mod Size) <> 0) then
        Inc (Result);
      end;
  end;

  function Modulo (X, N :integer) :integer;
  begin
    if (X >= N) then
      Result := X mod N
    else
      Result := N - ((-X) mod N);
  end;

  function CEN (Size, MaxSize :integer) :integer;
  begin
    Result := SUCC ((MaxSize - Size) DIV 2);
  end;

  function BTW (Center, Left, Right :integer) :boolean;
  begin
    Result := (Center >= Left) and (Center <= Right);
  end;

  procedure SET_BASE (NewBase :Base_);
  var
    i :byte;
  begin
    CurrentNumericBase := NewBase;
    Digits := '';
    for i := 0 to PRED (CurrentNumericBase) do
      Digits := Digits + Digit[i];
  end;

  function LIM (Width :integer) :integer;
  var
    i     :integer;
  begin
    Result := CurrentNumericBase;
    for i := 1 to PRED (Width) do
      Result := Result * CurrentNumericBase;
  end;

  function DIG (Ch :Char) :boolean;
  var
    i :byte;
  begin
    i := 0;
    while (i < CurrentNumericBase) and (Digit[i] <> Ch) do INC (i);
    DIG := (i < CurrentNumericBase);
  end;

  function WID (aValue :integer) :ShortInt;
  begin
    Result := 0;
    repeat
      aValue := aValue DIV CurrentNumericBase;
      Inc (Result);
      until (aValue = 0);
  end;

  function CommaSeparated (const aValue :string) :string;
  var
    {LS, L2    :integer;}
    i         :integer;
    {Temp      :string;}
    q         :integer;
  begin
    {
    Result := aValue;
    LS := Length (Result);
    L2 := (LS - 1) div 3;
    Temp := '';
    for i := 1 to L2 do
      Temp := ThousandSeparator + System.Copy (Result, LS - 3 * i + 1, 3) + Temp;
    Result := System.Copy (Result, 1, (LS - 1) mod 3 + 1) + Temp;
    if (Result <> '') and (Result[1] = ThousandSeparator) then
      System.Delete (Result, 1, 1);
    }
    Result := '';
    q := 0;
    for i := Length (aValue) downto 1 do begin
      Inc (q);
      if (q = 3) then
      end;
  end;

  function PercentOf (aValue, aPct :cardinal) :cardinal;
  begin
    Result := Round ((aValue * aPct) / 100.0);
  end;

  function PercentOf (aValue, aPct :word) :word;
  begin
    Result := Round ((aValue * aPct) / 100.0);
  end;

  function PercentOf (aValue, aPct :integer) :integer;
  begin
    Result := Round ((aValue * aPct) / 100.0);
  end;

  function PercentOf (aValue, aPct :double) :integer;
  begin
    Result := Round ((aValue * aPct) / 100.0);
  end;

  function Ratio (a, b :double) :double;
  begin
    Result := 0;
    if (b <> 0) then
      Result := (a / b);
  end;

  function Percent (a, b :double) :double;
  begin
    Result := Ratio (a, b ) * 100;
  end;

  function PercentStr (a, b :double) :string;
  begin
    Result := FloatToStrF (Percent (a, b), ffFixed, 3, 0) + '%';
  end;


  (*function PctStr (aDiff, aBase :integer) :string;
  begin
    if (aDiff = 0) or (aBase = 0) then
      Result := '0%'
    else
      Result := FloatToStrF ((aDiff / aBase) * 100, ffFixed, 3, 0) + '%';
    Result := Str__.LR (5, '', Result);
  end;*)

  function FloatStr (aValue :double) :string;
  begin
    Result := FloatToStrF (aValue, ffFixed, 6, 2);
    //Result := LR (6, '', Result);
  end;

var
   {$if CompilerVersion < 22}
    Save :Char = '.';
   {$else}
    Save :Char = '.';
   {$ifend}


  procedure Floating (aValue :boolean);
  begin
    (*
    if aValue then begin
     {$if CompilerVersion < 22}
      Save := SysUtils.DecimalSeparator;
      SysUtils.DecimalSeparator := Char0.Dot;
     {$else}
      Save := FormatSettings.DecimalSeparator;
      SysUtils.DecimalSeparator := Char0.Dot;
     {$ifend}
      end
    else
      SysUtils.DecimalSeparator := Save;
    *)
  end;

  function FloatToStr (aValue :double) :string;
  begin
    Floating (true);
    Result := SysUtils.FloatToStr (aValue);
    Floating (false);
  end;

  function StrToFloatDef (aValue :string; aDef :double) :double;
  begin
    if Str__.IsBlank (aValue) then
      Result := aDef
    else
      Result := SysUtils.StrToFloat (SysUtils.Trim (aValue), StdSettings);
  end;

  function StrToInt (aValue :string) :integer;
  begin
    Result := 0;
    if (aValue <> '') then
      try
        Result := SysUtils.StrToInt (aValue);
      except
        on e :Exception do
          //ShowMessage (aValue);
        end;
  end;

  function IntToStr2 (aValue :integer) :string;
  begin
    Result := '';
    if (aValue <> 0) then
      Result := SysUtils.IntToStr (aValue);
  end;

  function MoneyToStr (aValue :integer) :string;
  begin
    Result := '';
    if (aValue <> 0) then begin
      Result := IntToStr (aValue);
      System.Insert (Dot, Result, Length (Result)-1);
      end;
  end;

  function StrToMoney (aValue :string) :integer;
  var
    i   :integer;
  begin
    Result := 0;
    if (aValue <> '') then begin
      i := System.Pos (Dot, aValue);
      if (i > 0) then
        System.Delete (aValue, i, 1);
      Result := StrToInt (aValue);
      end;
  end;



  constructor FloatValue__.Create;
  begin
    inherited Create;
  end;

  destructor FloatValue__.Destroy;
  begin
    inherited Destroy;
  end;

  function FloatValue__.FromString (aValue :string) :double;
  var
    i  :integer;
  begin
    if not _Decided then begin
      i := Length (aValue);
      while (i > 0) and (aValue[i] in DigitsSet) do
        Dec (i);
      if (i >= 1) then begin
        if (aValue[i] <> FormatSettings.DecimalSeparator) then
          aValue[i] := FormatSettings.DecimalSeparator;
        end;
      end;
    System.Val (aValue, Result, i);
    if (i <> 0) then
      Result := 0;
  end;

  class function FloatValue__.DecimalPower(aDecimals :Decimals_) :double;
  begin
    case aDecimals of
      0:   Result := 1.0;
      1:   Result := 10.0;
      2:   Result := 100.0;
      3:   Result := 1000.0;
      else Result := Math.Power(10, aDecimals);
    end;
  end;

  class function FloatValue__.RoundTo (aValue :double; aDecimals :Decimals_) :double;
  var
    Pwr :double;
  begin
    Pwr := DecimalPower (aDecimals);
    if (aValue > 0) then
      Result := int(aValue * Pwr + 0.5) / Pwr
    else if (aValue < 0) then
      Result := int(aValue * Pwr - 0.5) / Pwr
    else
      Result := 0;
  end;


initialization
  SET_BASE (Decimal);
  FloatValue := FloatValue__.Create;
  GetLocaleFormatSettings (GetThreadLocale, StdSettings);
  StdSettings.DecimalSeparator := '.';
  StdSettings.ThousandSeparator := ',';
finalization
  FreeAndNil (FloatValue);
end.




