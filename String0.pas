unit String0;
{..$define FastStrings}
{$H+}
interface
uses
  Classes,
  //JclStrings,
  Char0;
const
  CommentOpen = '/*';
  CommentClose = '*/';
type
  UniqStr__ = class (TStringList)
   private
   protected
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    end;

  Str__ = class (TStringList)
   private
    function GetTheLast :integer;
   protected
    _Internal :boolean;
    function Get (Index :integer): string;
      //override;
    procedure Put (Index :integer; const S: string);
      //override;
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    function Load (const aFileName :string) :boolean;
      virtual;
    function Save (const aFileName :string) :boolean; 
    procedure InitSorted (aValues :array of string);
    property TheLast :integer read GetThelast;
    class function Ch (const Line :string; i :integer) :Char;
    class procedure ResetLen (var s :string);
    class function Val (out Res :integer; const Src :string) :boolean;
    class function NU (aValue :integer) :string;
    class function IsBlank (const aValue :string) :boolean;
    class function Bracketed (aValue :string) :string;
    class function Pos (const aWhat, aWhere :string; Start :integer = 1; NoCase :boolean = false) :integer;
    class function FindFirst (var aPos :integer; aStr :string; Chars :CharSet_) :boolean;
    class function StripBetween (Opening, Closing :Char; Src :string) :string;
    class procedure UpdateLength (var s :string);
    class function Quoted (const Quotes :Quotes_; const s :string) :string;
    class function Commented (s :string) :string;
    class function Comment (s :string) :string;
    class function UnQuoted (const Quotes :Quotes_; const s :string) :string;
    class function EnsureLast (var Target :Widestring; aCh :Char; Need :boolean) :boolean;
      overload;
    class function EnsureLast (var Target :string; aCh :Char; Need :boolean) :boolean;
      overload;
    class function EnsureLast (var Target :string; s :string; Need :boolean) :boolean;
      overload;
    class function EnsureFirst (var Target :string; s :string; Need :boolean) :boolean;
    class function EnsureNoTrailing (var Target :string; Sep :Char) :boolean;
    class function FilteredOut (aStr :string; BadChars :CharSet_; Replace :Char = #0) :string;
    class function Filtered (aStr :string; GoodChars :CharSet_) :string;
    class procedure Append (var aTarget :string; aSep :Char; aSource :string);
    class procedure AppStr (var aTarget :string; aSep, aSource :string);
    class function IsIn (aWhat, aWhere :string; NoCase :boolean = false) :boolean;
    class function IsInW (aWhat, aWhere :string; NoCase :boolean = false) :boolean;
    class function FindAmong (var i :integer; const What :string; const Where :array of string) :boolean;
    class function IsOneOf (aLine :string; const aSubLines :array of string) :boolean;
    class function IsLocated (SubLine, Line :String; Start :integer) :boolean;
    class function AreLocated (const aSubLines :array of string; aLine :string; aStart :integer) :boolean;
    class function ContainsOneOf (aLine :string; const aSubLines :array of string; NoCase :boolean = false) :boolean;
    class function PosCh (aWhat :Char; aWhere :string; Start :integer = 1) :integer;
    class function Replace (aSource, aFind, aReplace :string; aCaseSensitive :boolean = false) :string;
    class function Subst (var Line :string; const Src, Tgt :string) :boolean;
    class function LR (Width :integer; Left, Right :string) :string;
    class function CenterCh (S :string; aCh :Char; Width :integer) :string;
    class function CharStr (aCh :Char; Len :integer) :string;
    class function Offset (const s :string) :integer;
    class function Skip (const aWhat, aLine :string; aStart :integer; MaxCount :integer = 0) :integer;
    class function Locate (aWhat, aLine :string; aStart :integer) :integer;
    class function Next (Delim :Chars_; const Source :string; Start, Finish :integer) :integer;
    class function Reverse (var Source :string) :boolean;
    class function Numbered (aPrefix :string; var aSuffix :SmallInt) :string;
    class function Match (Templ, Name :string; UseCase :boolean) :boolean;
    class function CharCount (const aSource: string; const c: Char; StartPos :integer = 1): integer;
    class function IsSafe (const s :string) :boolean;
    end;

  WordStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function Capitalized (const aValue :string) :string;
    end;

  HexStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function Encode (aValue :string) :string;
    class function Decode (aValue :string) :string;
    end;

  NumStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function Width (aValue :integer) :ShortInt;
    class function Percent (aDiff, aBase :integer) :string;
    class function IsDigital (const s :string) :boolean;
    class function AsString (aValue :Integer; Width :byte; NumericBase :byte = 10) :string;
    class function AsFloat (aValue :string) :double;
    end;

  HtmlStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function GetQuotedChar (aValue :string) :Char;
    class function StripEscapes (aValue :string) :string;
    class function StripTags (aValue :string) :string;
    end;

  HttpStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function GetPath (aValue :string) :string;
    end;

  PathStr__ = class (Str__)
   private
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    class function ExtractFileNameBase (aValue :string) :string;
    class function SpacesOk (aSpec :string) :string;
    class function Usable (aName :string) :string;
    class function CommonDir (aStr, bStr :string) :string;
    end;

  function TrimRight (aValue :string) :string;

implementation
uses
 {$ifdef FastStrings}
  FastStrings,
 {$else}
  StrUtils,
 {$endif}
  Event0,
  a0,
  SysUtils;

  constructor UniqStr__.Create;
  begin
    inherited Create;
    Sorted := true;
    Duplicates := dupIgnore;
    CaseSensitive := false;
  end;

  destructor UniqStr__.Destroy;
  begin
    inherited Destroy;
  end;




  constructor Str__.Create;
  begin
    inherited Create;
  end;

  destructor Str__.Destroy;
  begin
    inherited Destroy;
  end;

  function Str__.GetTheLast :integer;
  begin
    Result := Pred (Count);
  end;

  function Str__.Get (Index: Integer): string;
  begin
    Result := '';
    if (Index < Count) then
      //Result := Trim (inherited Get (Index));
      Result := inherited Get (Index);
  end;

  function Str__.Load (const aFileName :string) :boolean;
  begin
    try
      inherited LoadFromFile (aFileName);
      Result := true;
    except
      on e :Exception do begin
        Result := false;
        end;
    end;
  end;

  function Str__.Save (const aFileName :string) :boolean;
  begin
    try
      inherited SaveToFile (aFileName);
      Result := true;
    except
      on e :Exception do begin
        Result := false;
        end;
    end;
  end;


  procedure Str__.Put (Index :Integer; const S: string);
  var
    Empty :boolean;
    i     :integer;
  begin
    Empty := false;
    if (s = '') then begin
      Empty := true;
      i := 0;
      while Empty and (i < Count) do
        if inherited Get (i) <> '' then
          Empty := false
        else
          Inc (i);
      end;
    if Empty then
      Event0.Skip
    else begin
      _Internal := true;
      while (Index >= Count) do
        Add ('');
      _Internal := false;
      inherited Put (Index, s);
      end;
  end;

  procedure Str__.InitSorted (aValues :array of string);
  var
    i :integer;
  begin
    Sorted := true;
    CaseSensitive := true;
    Clear;
    for i := Low (aValues) to High (aValues) do
      Add (aValues[i]);
  end;

  class function Str__.Numbered (aPrefix :string; var aSuffix :SmallInt) :string;
  begin
    Inc (aSuffix);
    Result := SysUtils.Trim (aPrefix) + SysUtils.IntToStr (aSuffix);
  end;

  class function Str__.Ch (const Line :string; i :integer) :Char;
  var
    Len :integer;
  begin
    Result := #0;          //Space
    Len := System.Length (Line);
    if (i < 0) then                       {-1 = last pos}
      i := Len + i + 1;
    if (i >= 1) and (i <= Len) then
      Result := Line[i];
  end;

  class procedure Str__.ResetLen (var s :string);
  begin
    System.SetLength (s, SysUtils.StrLen (PChar(s)));
  end;

  class function Str__.Val (out Res :integer; const Src :string) :boolean;
  var
    Code   :integer;
  begin
    Result := false;
    System.Val (Src, Res, Code);
    if (Code = 0) then
      Result := true;
  end;

  class function Str__.NU (aValue :integer) :string;
  begin
    System.Str (aValue, Result);
  end;


  class function Str__.IsBlank (const aValue :string) :boolean;
  var
    i       :integer;
    Len     :integer;
  begin
    Result := true;
    i := 1;
    Len := System.Length (aValue);
    while Result and (i <= Len) do
      if (aValue[i] > Space) then
        Result := false
      else
        Inc (i);
  end;

  class function Str__.Quoted (const Quotes :Quotes_; const s :string) :string;
  var
    Len :integer;
  begin
    Len := System.Length (s);
    if (Len >= 2) and (s[1] = Quotes.Open) and (s[Len] = Quotes.Close) then //already
      Result := s
    else begin
      if (Quotes.Open = Quotes.Close) then
        ///Result := AnsiQuotedStr (s, Quotes.Open)
        Result := Quotes.Open + s + Quotes.Close
      else
        Result := Quotes.Open + s + Quotes.Close;
      end;
  end;

  class function Str__.Comment (s :string) :string;
  begin
    Result := CommentOpen + s + CommentClose;
  end;

  class function Str__.Commented (s :string) :string;
  begin
    Result := Space;
    Append (Result, Space, s);
    Append (Result, Space, Space);
    Result := Comment (Result);
  end;

  class function Str__.UnQuoted (const Quotes :Quotes_; const s :string) :string;
  //SysUtils.AnsiExtractQuotedStr
  var
    Len  :integer;
    i    :integer;
  begin
    Result := s;
    Len := System.Length (s);
    if (Len >= 2) then
      if (Result[1] = Quotes.Open) then
      if (Result[Len] = Quotes.Close) then begin
        System.Delete (Result, Len, 1);
        System.Delete (Result, 1, 1);
        if (Quotes.Open = Quotes.Close) then begin
          i := Length (Result);
          while (i > 1) do begin
            if (Result[i] = Quotes.Close) and (Result[Pred(i)] = Quotes.Open) then begin
              System.Delete (Result, i, 1);
              Dec (i);
              end;
            Dec (i);
            end;
          end;
        end;
  end;

  class function Str__.Bracketed (aValue :string) :string;
  begin
    Result := '';
    if not IsBlank (aValue) then
      Result := SquareBrackets.Open + aValue + SquareBrackets.Close;
  end;

  class procedure Str__.UpdateLength (var s :string);
  begin
    System.SetLength (s, StrLen (PChar(s)));
  end;

  class function Str__.Pos (const aWhat, aWhere :string; Start :integer = 1; NoCase :boolean = false) :integer;
  begin
   {$ifdef FastStrings}
    if NoCase then
      Result := FastPosNoCase (aWhere, aWhat, Length (aWhere), Length (aWhat), Start)
    else
      Result := FastPos (aWhere, aWhat, Length (aWhere), Length (aWhat), Start);
   {$else}
    if (Start = 1) then begin
      if NoCase then
        Result := System.Pos (UpperCase (aWhat), UpperCase (aWhere))
      else
        Result := System.Pos (aWhat, aWhere);
      end
    else begin
      if NoCase then
        Result := StrUtils.PosEx (UpperCase (aWhat), UpperCase (aWhere), Start)
      else
        Result := StrUtils.PosEx (aWhat, aWhere, Start);
      end;
   {$endif}
  end;

  class function Str__.IsIn (aWhat, aWhere :string; NoCase :boolean = false) :boolean;
  var
    p   :integer;
  begin
    Result := false;
    p := Pos (aWhat, aWhere, 1, NoCase);
    if (p > 0) then
      Result := true;
  end;

  class function Str__.IsInW (aWhat, aWhere :string; NoCase :boolean = false) :boolean;
  var
    i :integer;
  begin
    Result := false;
    i := Pos (aWhat, aWhere, 1, NoCase);
    while not Result and (i <> 0) do begin
      if ((i = 1) or not (aWhere[i-1] in Identifiers)) then
        if ((i = Succ (Length (aWhere)-Length(aWhat))) or not (aWhere[i+Length(aWhat)] in Identifiers)) then
          Result := true;
      if not Result then begin
        {System.Delete (aWhere, 1, i);
        i := Pos (aWhat, aWhere, 1, NoCase);}
        i := Pos (aWhat, aWhere, i + Length (aWhat), NoCase);
        end;
      end;
  end;

  class function Str__.IsLocated (SubLine, Line :String; Start :integer) :boolean;
  var
    i        :integer;
    SubLen   :integer;
    Len      :integer;
  begin
    Result := false;
    SubLen := System.Length (SubLine);
    Len := System.Length (Line);
    if (Start < 0) then                       {-1 = first right pos}
      Start := Len + Start + 1
    else if (Start = 0) then
      Start := Len - SubLen + 1;
    Dec (Start);            {make zero-based}
    if (Start < 0) then
      Start := 0;
    if ((Start + SubLen) <= Len) then begin
      Result := true;
      i := 1;
      while Result and (i <= SubLen) do
        if (SubLine[i] = Line[Start + i]) then
          Inc (i)
        else
          Result := false;
      end;
  end;

  class function Str__.IsOneOf (aLine :string; const aSubLines :array of string) :boolean;
  var
    i  :integer;
  begin
    Result := false;
    for i := Low (aSubLines) to High (aSubLines) do
      if (aSublines[i] = aLine) then begin
        Result := true;
        Exit;
        end;
  end;

  class function Str__.AreLocated (const aSubLines :array of string; aLine :string; aStart :integer) :boolean;
  var
    i  :integer;
  begin
    Result := false;
    for i := Low (aSubLines) to High (aSubLines) do
      if IsLocated (aSublines[i], aLine, aStart) then begin
        Result := true;
        Exit;
        end;
  end;

  class function Str__.ContainsOneOf (aLine :string; const aSubLines :array of string; NoCase :boolean = false) :boolean;
  var
    i  :integer;
  begin
    Result := false;
    for i := Low (aSubLines) to High (aSubLines) do
      if IsIn (aSublines[i], aLine, NoCase) then begin
        Result := true;
        Exit;
        end;
  end;

  class function Str__.FindAmong (var i :integer; const What :string; const Where :array of string) :boolean;
  begin
    Result := false;
    i := 0;
    while not Result and (i <= High (Where)) do begin
      if SameText (What, Where[i]) then
        Result := true
      else
        Inc (i);
      end;
  end;

  class function Str__.FindFirst (var aPos :integer; aStr :string; Chars :CharSet_) :boolean;
  var
    i    :integer;
    Len  :integer;
  begin
    Result := false;
    Len := Length (aStr);
    i := 1;
    while not Result and (i <= Len) do
      if (aStr[i] in Chars) then begin
        aPos := i;
        Result := true;
        end
      else
        Inc (i);
  end;

  class function Str__.EnsureLast (var Target :WideString; aCh :Char; Need :boolean) :boolean;
  var
    Len  :integer;
  begin
    Result := false;
    Len := Length (Target);
    if (Len > 0) and (Target[Len] = WideChar (aCh)) then begin
      if not Need then begin
        System.Delete (Target, Len, 1);
        Result := true;
        end;
      end
    else begin
      if Need then begin
        Target := Target + aCh;
        Result := true;
        end;
      end;
  end;

  class function Str__.EnsureLast (var Target :string; aCh :Char; Need :boolean) :boolean;
  var
    Len  :integer;
  begin
    Result := false;
    Len := Length (Target);
    if (Len > 0) and (Target[Len] = aCh) then begin
      if not Need then begin
        System.Delete (Target, Len, 1);
        Result := true;
        end;
      end
    else begin
      if Need then begin
        Target := Target + aCh;
        Result := true;
        end;
      end;
  end;

  class function Str__.EnsureLast (var Target :string; s :string; Need :boolean) :boolean;
  begin
    Result := false;
    if IsLocated (s, Target, -Length (s)) then begin
      if not Need then begin
        System.Delete (Target, Length (Target) - Length (s) + 1, Length (s));
        Result := true;
        end;
      end
    else begin
      if Need then begin
        Target := Target + s;
        Result := true;
        end;
      end;
  end;

  class function Str__.EnsureFirst (var Target :string; s :string; Need :boolean) :boolean;
  begin
    Result := false;
    if IsLocated (s, Target, 1) then begin
      if not Need then begin
        System.Delete (Target, 1, Length (s));
        Result := true;
        end;
      end
    else begin
      if Need then begin
        Target := s + Target;
        Result := true;
        end;
      end;
  end;

  class function Str__.EnsureNoTrailing (var Target :string; Sep :Char) :boolean;
  begin
    Result := Str__.EnsureLast (Target, Sep, false);
  end;

  class function Str__.StripBetween (Opening, Closing :Char; Src :string) :string;
  var
    BeginPos :integer;
    EndPos   :integer;
    i        :integer;
  begin
    repeat
      EndPos := PosCh (Closing, Src, 1);
      BeginPos := 0;
      i := EndPos;
      while (EndPos > 0) and (i > 0) do
        if (Src[i] = Opening) then begin
          BeginPos := i;
          i := 0;
          end
        else
          Dec (i);
      if (EndPos > BeginPos) then
        System.Delete (Src, BeginPos, EndPos-BeginPos+1);
    until (BeginPos = 0);
    i := PosCh (Opening, Src, 1);
    if (i <> 0) then
      System.Delete (Src, i, Length (Src));
    Result := Src;
  end;

  class function Str__.FilteredOut (aStr :string; BadChars :CharSet_; Replace :Char = NullCh) :string;
  var
    SrcLen :integer;
    TgtLen :integer;
    i      :integer;
    Chr     :Char;
  begin
    SrcLen := Length (aStr);
    SetLength (Result, SrcLen);
    TgtLen := 0;
    for i := 1 to SrcLen do begin
      Chr := aStr[i];
      if (Chr in BadChars) then begin
        if (Replace <> NullCh) then begin    //skip
          Inc (TgtLen);
          Result[TgtLen] := Replace;
          end;
        end
      else begin
        Inc (TgtLen);
        Result[TgtLen] := Chr;
        end;
      end;
    if (TgtLen <> SrcLen) then
      SetLength (Result, TgtLen);
  end;

  class function Str__.Filtered (aStr :string; GoodChars :CharSet_) :string;
  var
    SrcLen :integer;
    TgtLen :integer;
    i      :integer;
    Chr    :Char;
  begin
    SrcLen := Length (aStr);
    SetLength (Result, SrcLen);
    TgtLen := 0;
    for i := 1 to SrcLen do begin
      Chr := aStr[i];
      if (Chr in GoodChars) then begin
        Inc (TgtLen);
        Result[TgtLen] := Chr;
        end;
      end;
    if (TgtLen <> SrcLen) then
      SetLength (Result, TgtLen);
  end;

  class procedure Str__.Append (var aTarget :string; aSep :Char; aSource :string);
  var
    Left, Right :record
      Ok  :boolean;
      Len :integer;
      end;
  begin
    Left.Ok := false;
    Left.Len := System.Length (aTarget);
    if (Left.Len > 0) then
      if (aTarget[Left.Len] = aSep) then
        Left.Ok := true;
    Right.Ok := false;
    Right.Len := System.Length (aSource);
    if (Right.Len > 0) then
      if (aSource[1] = aSep) then
        Right.Ok := true;
    if (Left.Ok = Right.Ok) then begin
      if Left.Ok then
        System.Delete (aTarget, Left.Len, 1)
      else begin
        if (Left.Len > 0) then
        if (Right.Len > 0) then
        if (aSep <> #0) then
          aTarget := aTarget + aSep;
        end;
      end;
    aTarget := aTarget + aSource;
  end;

  class procedure Str__.AppStr (var aTarget :string; aSep, aSource :string);
  var
    Left, Right :record
      Ok  :boolean;
      Len :integer;
      end;
    SepLen :integer;
  begin
    SepLen := Length (aSep);
    Left.Ok := false;
    Left.Len := System.Length (aTarget);
    if IsLocated (aSep, aTarget, -SepLen) then
      Left.Ok := true;
    Right.Ok := false;
    Right.Len := System.Length (aSource);
    if IsLocated (aSep, aSource, 1) then
      Right.Ok := true;
    if (Left.Ok = Right.Ok) then begin
      if Left.Ok then
        System.Delete (aTarget, Left.Len-SepLen+1, SepLen)
      else begin
        if (Left.Len > 0) then
        if (Right.Len > 0) then
          aTarget := aTarget + aSep;
        end;
      end;
    aTarget := aTarget + aSource;
  end;

  class function Str__.PosCh (aWhat :Char; aWhere :string; Start :integer = 1) :integer;
  begin
   {$ifdef FastStrings}
   Result := FastStrings.FastCharPos (aWhere, aWhat, Start);
   {$else}
   Assert (Start = 1);
   Result := System.Pos (aWhat, aWhere);
   {$endif}
  end;

  class function Str__.Replace (aSource, aFind, aReplace :string; aCaseSensitive :boolean = false) :string;
  begin
   {$ifdef FastStrings}
    Result := FastStrings.FastReplace (aSource, aFind, aReplace, aCaseSensitive);
   {$else}
    if aCaseSensitive then
      Result := SysUtils.StringReplace (aSource, aFind, aReplace, [rfReplaceAll])
    else
      Result := SysUtils.StringReplace (aSource, aFind, aReplace, [rfReplaceAll, rfIgnoreCase]);
   {$endif}
  end;

  class function Str__.Subst (var Line :string; const Src, Tgt :string) :boolean;
  var
    p       :integer;
  begin
    Result := false;
    if (Src <> Tgt) then begin
      p := Pos (Src, Line);
      if (p >= 1) then begin
        System.Delete (Line, p, LENGTH (Src));
        System.Insert (Tgt, Line, p);
        Result := true;
        end;
      end;
  end;

  class function Str__.LR (Width :integer; Left, Right :string) :string;
  var
    l      :integer;
    r      :integer;
    i      :integer;
  begin
    l := Length (Left);
    r := Length (Right);
    if (l > Width) then
      l := Width;
    if (r > Width) then
      r := Width;
    Result := Left;
    SetLength (Result, Width);
    for i := Succ (l) to (Width - r) do
      Result[i] := Space;
    l := Width;
    r := Length (Right);
    while (r > 0) and (l > 0) do begin
      Result[l] := Right[r];
      Dec (l);
      Dec (r);
      end;
  end;

  class function Str__.CenterCh (S :string; aCh :Char; Width :integer) :string;
  var
    Src   :integer;
    Tgt   :integer;
    Len   :integer;
    i     :integer;
  begin
    Len := Length (s);
    SetLength (Result, Width);
    Tgt := 1;
    Src := 1;
    if (Len < Width) then begin
      Tgt := (Width - Len) div 2;
      if (Tgt < 1) then
        Tgt := 1;
      end
    else begin
      Src := (Len - Width) div 2;
      if (Src < 1) then
        Src := 1;
      end;
    if (Len > Width) then
      Len := Width;
    for i := 1 to Pred (Tgt) do
      Result[i] := aCh;
    for i := 1 to Len do
      Result[Tgt+i-1] := s[Src+i-1];
    for i := Tgt+Len to Width do
      Result[i] := aCh;
  end;

  class function Str__.CharStr (aCh :Char; Len :integer) :string;
  begin
    SetLength (Result, Len);
    if (Len > 0) then
      FillChar (Result[1], Len, Ord (aCh));
  end;

  class function Str__.Offset (const s :string) :integer;
  begin
    Result := 0;
    if Length (s) > 0 then begin
      Result := 1;
      while (Result <= Length (s)) and (s[Result] = Space) do
        Inc (Result);
      end;
  end;

  class function Str__.Skip (const aWhat, aLine :string; aStart :integer; MaxCount :integer = 0) :integer;
  var
    Len     :integer;
    ToSkip  :integer;
  begin
    Result := aStart;
    Len := System.Length (aLine);
    if (Result > Len) then
      Result := Succ (Len)
    else if (Result > 0) then begin
      ToSkip := MaxCount;
      if (ToSkip = 0) then
        ToSkip := High(ToSkip);
      while (ToSkip > 0) and (Result <= Len) and (0 <> PosCh (aLine[Result], aWhat)) do begin
        Dec (ToSkip);
        Inc (Result);
        end;
    end;
  end;

  class function Str__.Locate (aWhat, aLine :string; aStart :integer) :integer;
  var
    Found   :boolean;
    Len     :integer;
  begin
    Result := aStart;
    if (Result < 1) then
      Result := 1;
    Len := System.Length (aLine);
    Found := false;
    while not Found and (Result <= Len) do begin
      if (0 = PosCh (aLine[Result], aWhat)) then
        Inc (Result)
      else
        Found := true;
      end;
  end;

  class function Str__.Next (Delim :Chars_; const Source :string; Start, Finish :integer) :integer;
  var
    Ch :Char;
  begin
    while (Start <= Finish) do begin
      Ch := Source[Start];
      if (Ch in Delim) then
        Break;
      Inc (Start);
      if (Start > Finish) then
        Break;
      if (Ch = DoubleQuotes.Open) then
        repeat
          if (Start > Finish) then
            Break;
          Ch := Source[Start];
          Inc (Start);
        until (Ch = DoubleQuotes.Close)
      (*
      else if (Ch = Parentheses.Open) then
        Start := Next ([Parentheses.Close], Source, Start, Finish);
      else if (Ch = SquareBrackets.Open) then
        Start := Next ([SquareBrackets.Close], Source, Start, Finish)
      else if (Ch = AngleBrackets.Open) then
        Start := Next ([AngleBrackets.Close], Source, Start, Finish)
      *)
      end;
    Result := Start;
  end;

  class function Str__.Reverse (var Source :string) :boolean;
  var
     i    :integer;
     j    :integer;
    Half  :integer;
     Chr  :Char;
  begin
    Result := false;
    j := Length (Source);
    if (j >= 2) then begin
      Half := j div 2;
      for i := 1 to Half do begin
        Chr := Source[i];
        Source[i] := Source[j];
        Source[j] := Chr;
        Dec (j);
        end;
      Result := true;
      end;
  end;

  class function Str__.Match (Templ, Name :string; UseCase :boolean) :boolean;
  label 1;
  var
    Tlen, Nlen, t, n, i, j, Slen :integer;
    VarPart :boolean;

    function FIND (Tbeg, Tlen, Start :integer) :integer;
    var
      Finish   :integer;
      j        :integer;
      i        :integer;
      k        :integer;
      Equal    :boolean;
    begin
      Finish := (Nlen - Tlen) + 1;
      repeat
        j := 1;
        repeat
          i := Tbeg + j - 1;
          k := Start + j - 1;
          if (k > Nlen) then
            Equal := false
          else if (Templ[i] = One) then
            Equal := true
          else if (Templ[i] = Name[k]) then
            Equal :=  true
          else
            Equal := false;
          Inc (j);
        until (not Equal) or (j > Tlen);
        Start := SUCC (Start);
      until Equal or (Start > Finish);
      if Equal then
        Result := Start-1
      else
        Result := Nlen+1;
    end;

  begin
    Result := false;
    if not UseCase then begin
      Templ := SysUtils.UpperCase (Templ);
      Name := SysUtils.UpperCase (Name);
      end;
    Tlen := LENGTH (Templ);
    Nlen := LENGTH (Name);
    if (Tlen = 0) then
      Result := (Nlen = 0)
    else begin
      t := 1; n := 1;
      VarPart := false;
      while (t <= Tlen) and (n <= Nlen) do begin
        if (Templ[t] = Any) then begin
          VarPart := true;
          t := SUCC (t);
          end
        else if VarPart then begin
          VarPart := false;
          i := t;
          while (i <= Tlen) and (Templ[i] <> Any) do
            i := SUCC (i);
          Slen := i - t;
          j := FIND (t, Slen, n);
          if (j > Nlen) then goto 1
          else begin
            t := t + Slen;
            n := j + Slen;
            end;
          end
        else if (Templ[t] = Name[n]) or (Templ[t] = One) then begin
          t := SUCC (t);      n := SUCC (n);
          end                      
        else goto 1;
        end;                    {in strings}
      if (t > Tlen) then begin
        Result := (Templ[t-1] = Any)	{last * matches anything}
	              or (n > Nlen);		{both exhausted}
        end
      else {name exhausted earlier}
      	Result := (t = Tlen) and (Templ[t] = Any) and (n > Nlen);
      end;
    1:
  end;

  class function Str__.CharCount (const aSource: string; const c: Char; StartPos :integer = 1): integer;
  begin
    Assert (StartPos > 0);
    Result := 0;
    while (StartPos <= Length (aSource)) do begin
      if (aSource[StartPos] = c) then
        Inc (Result);
      Inc (StartPos);
      end;
  end;

  class function Str__.IsSafe (const s :string) :boolean;
  var
    Len :integer;

    function IsPresent (c :Char) :boolean;
    var
      i   :integer;
    begin
      i := Str__.PosCh (c, s);
      if (i > 1) and (i < Len) then
        Result := true
      else
        Result := false
    end;

  begin
    Result := true;
    Len := Length (s);
    if (Len = 0) then
      Result := false
    else if (s[1] <> DoubleQuote) and (s[Len] <> DoubleQuote) then begin
      if IsPresent (Comma) then
        Result := false
      else if IsPresent (SingleQuote) then
        Result := false
      end;
  end;



  constructor WordStr__.Create;
  begin
    inherited Create;
  end;

  destructor WordStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function WordStr__.Capitalized (const aValue :string) :string;
  var
    Len :integer;
  begin
    Len := System.Length (aValue);
    System.SetLength (Result, Len);
    if (Len > 0) then
      Result:= AnsiUpperCase (aValue[1]) + AnsiLowerCase (Copy (aValue, 2, MaxInt));
  end;





  constructor HexStr__.Create;
  begin
    inherited Create;
  end;

  destructor HexStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function HexStr__.Encode (aValue :string) :string;
  var
    Len :integer;
    i   :integer;
    j   :integer;
    b   :byte;
    x   :byte;
    y   :byte;
  begin
    Len := System.Length (aValue);
    System.SetLength (Result, Len * 2);
    j := 1;
    for i := 1 to Len do begin
      b := Ord (aValue[i]);
      x := (b shr 4) and $F;
      y := b and $F;
      Result[j] := HexDigitsStr[Succ (x)];
      Result[Succ(j)] := HexDigitsStr[Succ (y)];
      Inc (j, 2);
      end;
  end;

  class function HexStr__.Decode (aValue :string) :string;
  const
    DefaultHexDigit = '0';
  var
    Len :integer;
    i   :integer;
    j   :integer;

    function GetHexValue (Ch :Char) :byte;
    var
      Base :byte;
    begin
      if not (Ch in HexDigits) then
        Ch := DefaultHexDigit;
      if (Ch > '9') then begin
        Ch := UpCase (Ch);
        Base:= Ord ('A');
        end
      else
        Base := Ord ('0');
      Result := Ord (Ch) - Base;
    end;

  begin
    Len := System.Length (aValue);
    if Odd (Len) then begin
      aValue := DefaultHexDigit + aValue;
      Len := System.Length (aValue);
      end;
    System.SetLength (Result, Len Div 2);
    i := 1;
    j := 1;
    while (i <= Len) do begin
      Result[j] := Char ((GetHexValue (aValue[i]) shl 4) or (GetHexValue (aValue[Succ(i)]) and $F));
      Inc (i, 2);
      Inc (j);
      end;
  end;






  constructor NumStr__.Create;
  begin
    inherited Create;
  end;

  destructor NumStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function NumStr__.Width (aValue :integer) :ShortInt;
  begin
    Result := A0.WID (aValue);
  end;

  class function NumStr__.Percent (aDiff, aBase :integer) :string;
  const
    Precision = 3;
    Digits = 1;
  begin
    if (aDiff = 0) or (aBase = 0) then
      Result := '0'
    else
      Result := FloatToStrF ((aDiff / aBase) * 100, ffFixed, Precision, Digits);
    Result := LR (5, '', Result + PercentChar);
  end;

  class function NumStr__.IsDigital (const s :string) :boolean;
  var
    i   :integer;
    Len :integer;
    Chr  :Char;
    Ds  :boolean;
  begin
    Result := false;
    Ds := false;
    Len := System.Length (s);
    if (Len > 0) then begin
      Result := true;
      i := 1;
      while Result and (i <= Len) do begin
        Chr := s[i];
        if A0.DIG (Chr) then
          Inc (i)
        else if (Char(Chr) = FormatSettings.DecimalSeparator) then begin
          Inc (i);
          if Ds then
            Result := false;
          Ds := true;
          end
        else if (Char(Chr) = FormatSettings.ThousandSeparator) then
          Inc (i)
        else
          Result := false;
        end;
      end;
  end;

  class function NumStr__.AsString (aValue :longint; Width :byte; NumericBase :byte = 10) :string;
  var
    ADigit   :Char;
    Negative :boolean;
    Done     :boolean;
    i        :integer;
  begin
    Result := '';
    if (Width > NumStrMax) then
      Width := NumStrMax;

    Negative := (aValue < 0);
    if Negative then
      aValue := System.ABS (aValue);
    Done := false;
    repeat
      if (aValue = 0) and (Result <> '') then
        ADigit := LeadingSign
      else
        ADigit := Digit[aValue MOD NumericBase];
      SYSTEM.INSERT (ADigit, Result, 1);
      aValue := aValue DIV NumericBase;
      if (System.Length (Result) >= Width) then
        Done := true;
    until Done;

    if Negative then begin
      i := 1;
      while (i < LENGTH (Result)) and ((Result[i] = LeadingSign) or (Result[i] = '0'))
      do
        INC (i);
      if (i > 1) then
        DEC (i);
      Result[i] := Minus;
      end;
  end;

  class function NumStr__.AsFloat (aValue :string) :double;
  begin
    Result := StrToFloat (aValue, StdSettings);
  end;




  constructor HtmlStr__.Create;
  begin
    inherited Create;
  end;

  destructor HtmlStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function HtmlStr__.GetQuotedChar (aValue :string) :Char;
  const
    Literals :array [0..3] of string = ('lt', 'gt', 'amp', 'quot');
    Characters :array [0..3] of Char = (AngleOpen, AngleClose, Ampersand, DoubleQuote);
  var
    Len   :integer;
    Code  :word;
    s     :string;
    i     :integer;
  begin
    Result := Space;
    Len := System.Length (aValue);
    if (Len >= 1) then begin
      if (aValue[1] = Sharp) then begin  //numeric
        if (Len >= 2) then begin
          i := 2;
          if (aValue[2] in ['h', 'H']) then  //hex
            s := '$' + System.Copy (aValue, 3, 100)
          else
            s := System.Copy (aValue, 2, 100);
          Code := StrToIntDef (s, Ord (Space));
          Result := Char (Code);
          end;
        end
      else begin
        aValue := LowerCase (aValue);
        if FindAmong (i, aValue, Literals) then
          Result := Characters[i];
        end;
      end;
  end;

  class function HtmlStr__.StripEscapes (aValue :string) :string;
  var
    s      :integer;
    t      :integer;
    Len    :integer;
    Chr     :Char;
    IsQuoted :boolean;
    Quote  :string;
  begin
    Len := System.Length (aValue);
    SetLength (Result, Len);
    IsQuoted := false;
    Quote := '';
    s := 1;
    t := 0;
    while (s <= Len) do begin
      Chr := aValue[s];
      if IsQuoted then begin
        if (Chr = SemiColon) then begin
          IsQuoted := false;
          Chr := GetQuotedChar (Quote);
          Inc (t);
          Result[t] := Chr;
          Quote := '';
          end
        else
          Quote := Quote + Chr;
        end
      else begin
        if (Chr = Ampersand) then
          IsQuoted := true
        else begin
          Inc (t);
          Result[t] := Chr;
          end;
        end;
      Inc (s);
      end;
    SetLength (Result, t);
  end;

  class function HtmlStr__.StripTags (aValue :string) :string;
  begin
    Result := StripBetween (AngleOpen, AngleClose, aValue);
  end;

  

  constructor HttpStr__.Create;
  begin
    inherited Create;
  end;

  destructor HttpStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function HttpStr__.GetPath (aValue :string) :string;
  var
    i :integer;
  begin
    i := System.Length (aValue);
    if (i > 0) then begin
      while (i > 1) and (aValue[i] <> Slash) do
        Dec (i);
      if (aValue[i] = Slash) then
        System.Delete (aValue, i, MaxInt);
      end;
    Result := aValue;
  end;




  constructor PathStr__.Create;
  begin
    inherited Create;
  end;

  destructor PathStr__.Destroy;
  begin
    inherited Destroy;
  end;

  class function PathStr__.ExtractFileNameBase (aValue :string) :string;
  var
    St   :integer;
    Fi   :integer;
  begin
    Result := SysUtils.ExtractFileName (aValue);
    St := PosCh (BracketOpen, Result, 1);
    if (St > 0) then begin
      Fi := PosCh (BracketClose, Result, Succ (St));
      if (Fi > 0) then
        System.Delete (Result, St, Fi-St+1);
      end;
  end;

  class function PathStr__.SpacesOk (aSpec :string) :string;
  var
    i   :integer;
    Ok  :boolean;
  begin
    Result := aSpec;
    Ok := true;
    i := 1;
    while Ok and (i <= Length (aSpec)) do begin
      if (aSpec[i] in [Space, Ampersand]) or (Ord (aSpec[i]) > 127)  then
        Ok := false
      else
        Inc (i);
      end;
    if not Ok then
      Result := SysUtils.AnsiQuotedStr (Result, DoubleQuote);      //Result := Quoted (DoubleQuotes, Result);
  end;

  class function PathStr__.Usable (aName :string) :string;
  var
    i  :SmallInt;
    Chr :Char;
  begin
   {$ifdef SimpleFilter}
    Result := FilteredOut (aName, [BackSlash, Slash, Colon, Asterisk, Question, DoubleQuote, AngleOpen, AngleClose, PipeChar]);
   {$else}
    SetLength (Result, System.Length (aName));
    for i := 1 to System.Length (aName) do begin
      Chr := aName[i];
      if (Chr = BackSlash) then
        Chr := Space
      else if (Chr = Slash) then
        Chr := Space
      else if (Chr = Colon) then
        Chr := Minus
      else if (Chr = Asterisk) then
        Chr := Space
      else if (Chr = Question) then
        Chr := Space
      else if (Chr = DoubleQuote) then
        Chr := Space
      else if (Chr = AngleOpen) then
        Chr := ParenOpen
      else if (Chr = AngleClose) then
        Chr := ParenClose
      else if (Chr = PipeChar) then
        Chr := Space;
      Result[i] := Chr;
      end;
   {$endif}
  end;

  class function PathStr__.CommonDir (aStr, bStr :string) :string;
  const
   DirSep = Backslash;
  var
    LenA :integer;
    LenB :integer;
    Len  :integer;
    i    :integer;
  begin
    Len := 0;
    i := 1;
    LenA := System.Length (aStr);
    LenB := System.Length (bStr);
    while (i <= LenA) and (i <= LenB) and  (UpCase (aStr[i]) = UpCase (bStr[i])) do begin
      Inc (Len);
      Inc (i);
      end;
    {get rid of incomplete ending}
    i := Len;
    while (i > 1) and (aStr[i] <> DirSep) do
      Dec (i);
    Result := System.Copy (aStr, 1, i);
  end;





  function TrimRight (aValue :string) :string;
  var
    i       :integer;
    Finish  :integer;
  begin
    Result := '';
    Finish := Length (aValue);
    if (Finish > 0) then begin
      i := 1;
      while (i <= Finish) and (aValue[i] <> #0) do
        Inc (i);
      Finish := Pred (i);
      while (Finish > 0) and (aValue[Finish] <= Space) do
        Dec (Finish);
      Result := System.Copy (aValue, 1, Finish);
      end;
  end;


initialization
finalization
end.
