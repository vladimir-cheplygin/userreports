unit Version0;
interface
type
  Version_ = record
    case boolean of
      true: (Value :longint);
      false: (Minor :word; Major :word);
      end;

  Version__ = class (TObject)
   private
    _Value :Version_;
    FDebug :boolean;
    _Valid :boolean;
    _Extra :Version_;           //Release:Build
    procedure SetValue (aValue :longint);
    procedure SetMajor (aValue :word);
    procedure SetMinor (aValue :word);
    function GetAsString :string;
    function GetAsStr :string;
    procedure SetAsDateTime (aValue :TDateTime);
    procedure set_AsString (const aValue :string);
   protected
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    property Value :longint read _Value.Value write SetValue;
    property Major :word read _Value.Major write SetMajor;
    property Minor :word read _Value.Minor write SetMinor;
    property Debug :boolean read FDebug write FDebug;
    property IsValid :boolean read _Valid;
    property AsStr :string read GetAsStr;
    property AsString :string read GetAsString write set_AsString;
    function IsOlderThan (Other :Version__) :boolean;
    function Equals (Other :Version__) :boolean;
    property AsDateTime :TDateTime write SetAsDateTime;
    end;

implementation
uses
  SysUtils,
  Char0,
  Tk00;

var
  __Tokens :Tokens__;

  constructor Version__.Create;
  begin
    inherited Create;
  end;

  destructor Version__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure Version__.SetValue (aValue :longint);
  begin
    _Value.Value := aValue;
  end;

  procedure Version__.SetMajor (aValue :word);
  begin
    _Value.Major := aValue;
    _Valid := true;
  end;

  procedure Version__.SetMinor (aValue :word);
  begin
    _Value.Minor := aValue;
    _Valid := true;
  end;

  function Version__.GetAsStr :string;
  begin
    Result := IntToStr (_Value.Major) + Dot + IntToStr (_Value.Minor);
  end;

  function Version__.GetAsString :string;
  begin
    Result := AsStr + Dot +
      IntToStr (_Extra.Major) + Dot + IntToStr (_Extra.Minor);
  end;

  function Version__.IsOlderThan (Other :Version__) :boolean;
  begin
    if not IsValid then
      Result := false
    else if (Value < Other.Value) then
      Result := true
    else if (Value > Other.Value) then
      Result := false
    else if _Extra.Value < Other._Extra.Value then
      Result := true
    else
      Result := true
  end;

  function Version__.Equals (Other :Version__) :boolean;
  begin
    if (_Value.VAlue <> Other._Value.Value) then
      Result := false
    else if (_Extra.Value <> Other._Extra.VAlue) then
      Result := false
    else
      Result := true    
  end;

  procedure Version__.SetAsDateTime (aValue :TDateTime);
  var
    Base :TDateTime;
    Days :integer;
  begin
    Base := SysUtils.EncodeDate (2000, 1, 1);
    Days := Round (aValue - Base);
    Major := (Days Div 365);
    Minor := (Days Mod 365);
    _Extra.Value := 0;
  end;

  procedure Version__.set_AsString(const aValue :string);
  begin
    if (__Tokens = nil) then
      __Tokens := Tokens__.CreateInc(Dot);
    __Tokens.Line := aValue;
    if __Tokens.Next then
      Major := __Tokens.Number
    else
      _Value.Major := 0;
    if __Tokens.Next then
      Minor := __Tokens.Number
    else
      _Value.Minor := 0;
    if __Tokens.Next then
      _Extra.Major := __Tokens.Number
    else
      _Extra.Major := 0;
    if __Tokens.Next then
      _Extra.Minor := __Tokens.Number
    else
      _Extra.Minor := 0;
  end;

initialization
finalization
  FreeAndNil(__Tokens);
end.