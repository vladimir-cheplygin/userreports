unit BugReport1;
interface
uses
  System.Classes,
  BugReport0;

type
  TBugReport = class(BugReport__)
  private
    _Text   :TMemoryStream;
    _Image  :TMemoryStream;
    function get_Hash: Integer;
    procedure set_Text(const Value: TStream);
    procedure set_Image(const Value: TStream);
    function get_Image: TStream;
    function get_Text: TStream;
  public
    constructor Create; override;
    destructor Destroy; override;
    property Text: TStream read get_Text write set_Text;
    property Image: TStream read get_Image write set_Image;
    property Hash: Integer read get_Hash;
  end;

implementation
uses
  System.SysUtils,
  System.Hash;

constructor TBugReport.Create;
begin
  inherited Create;
end;

destructor TBugReport.Destroy;
begin
  inherited Destroy;
end;

function TBugReport.get_Hash: Integer;
begin
  Result := THashBobJenkins.GetHashValue(Unique);
end;

function TBugReport.get_Image: TStream;
begin
  if (_Text = nil) then
    _Text := TMemoryStream.Create;
  Result := _Text;
end;

function TBugReport.get_Text: TStream;
begin
  if (_Image = nil) then
    _Image := TMemoryStream.Create;
  Result := _Image;
end;

procedure TBugReport.set_Text(const Value: TStream);
begin
   if (Value = nil) then
     FreeAndNil(_Text)
  else begin
    if _Text = nil then
      _Text := TMemoryStream.Create;
    _Text.CopyFrom(Value);
  end;
end;

procedure TBugReport.set_Image(const Value: TStream);
begin
  if (Value = nil) then
    FreeAndNil(_Image)
  else begin
    if (_Image = nil) then
      _Image := TMemoryStream.Create;
    _Image.CopyFrom(Value);
  end;
end;


end.


