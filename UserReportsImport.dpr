program UserReportsImport;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  WinApi.Windows,
  System.SysUtils,
  System.IniFiles,
  Tim1,
  Session0;
var
  Run: Boolean;
  Moment: SimpleTimeout__;
  User: string;
  Pass: string;

  function RunOnce(const AUser, APass: string): Boolean;
  const
    Host = 'pop.gmail.com';
    Port = 992;
  var
    Session    :MailSession__;
  begin
    Result := True;
    try
      Session := MailSession__.Create;
      Session.Host := Host;
      Session.Port := Port;
      Session.User := AUser;
      Session.Pass := APass;
      Session.Recent := False;
      Session.JunkDelete := True;
      Session.ReportDelete := False;
      Session.Verbose :=  True;

      if not Session.IsConnected then begin
        if Session.Recoverable then
          WriteLn ('not connected')
        else
          Result := False;
      end
      else if Session.Process then begin
        with Session.Stats do
          if (Copied  > 0) then
            WriteLn (Format('reports copied/all: %d/%d', [Copied, Reports]));
      end
      else
        Result := False;

      Session.Free;
    except
      on E: Exception do
        Writeln(E.ClassName, ': ', E.Message);
    end;
  end;

  function KeyPressed(ExpectedKey :word) :boolean;
  var
    lpNumberOfEvents     :DWORD;
    lpBuffer             :TInputRecord;
    lpNumberOfEventsRead :DWORD;
    nStdHandle           :THandle;
  begin
    Result := false;
    nStdHandle := GetStdHandle(STD_INPUT_HANDLE);
    lpNumberOfEvents := 0;
    GetNumberOfConsoleInputEvents(nStdHandle, lpNumberOfEvents);
    if (lpNumberOfEvents <> 0) then begin
      PeekConsoleInput(nStdHandle, lpBuffer, 1, lpNumberOfEventsRead);
      if (lpNumberOfEventsRead <> 0) then
        if (lpBuffer.EventType = KEY_EVENT) then
          if lpBuffer.Event.KeyEvent.bKeyDown and ((ExpectedKey=0) or (lpBuffer.Event.KeyEvent.wVirtualKeyCode = ExpectedKey)) then
            Result := true
          else
            FlushConsoleInputBuffer(nStdHandle)
        else
          FlushConsoleInputBuffer(nStdHandle);
    end;
  end;

begin
  Moment := SimpleTimeout__.Create;
  Moment.AsMinutes := 10;
  if (ParamCount < 2) then begin
    WriteLn;
    WriteLn('UserReportsImport.exe <E-mail> <Password>');
    WriteLn;
    Exit;
  end
  else begin
    User := ParamStr(1);
    Pass := ParamStr(2);
  end;

  Run := true;
  while Run and RunOnce(User, Pass) do begin
    Moment.Active := true;
    while Moment.Active and Run do begin
      if KeyPressed (0) then
        Run := False
      else begin
        Sleep (999);
        Write(Format('(%d) press to stop', [Moment.Remaining]), #13);
      end;
    end;
    WriteLn('                                                   ', #13);
  end;
  Moment.Free;
end.
