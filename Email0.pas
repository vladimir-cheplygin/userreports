unit Email0;
interface
uses
  Classes,
  Contnrs,
  //List001,
  IdExplicitTLSClientServerBase,
  IdPOP3,
  IdIOHAndler,
  IdSSLOpenSSL,
  //CertSSL,
  IdMessage,
  IdAttachment,
  IdAttachmentFile,
  IdAttachmentMemory,
  String0,
  BugReport1;
type
   MailServer__ = class;
   ObjList__ = TObjectList;

   Email__ = class (TIdMessage)
   private
    _Server :MailServer__;
    _Index  :integer;
    _Text   :TIDAttachment;
    _Image  :TIDAttachment;
    _Report :TBugReport;
    procedure CreateAttEvent(const AMsg: TIdMessage; const AHeaders: TStrings; var AAttachment: TIdAttachment);
    function get_Report :TBugReport;
    function get_File (const aFileName :string) :TIDAttachment;
    function get_Text: TIDAttachment;
   protected
    function SubjectHasOneOf (Subjects: TStrings) :boolean;
    property Text :TIDAttachment read get_Text;
   public
     constructor Create;
     destructor Destroy;
       override;
     function IsIrrelevant :boolean;
     function IsBugReport :boolean;
     procedure DoLog (aText :string);
     function Save :boolean;
     function Delete :boolean;
     property Report :TBugReport read get_Report;
   end;

   Emails__ = class (ObjList__)
   protected
     function getItem (i :integer) :Email__;
   public
     property Items[i :integer] :Email__ read GetItem; default;
   end;

   MailServer__ = class (TIdPOP3)
   private
    ///_Ssl :TCertSSLIOHandler;
    _Ssl :TIdSSLIOHandlerSocketOpenSSL;
   private
    _Count        :integer;
    _Emails       :Emails__;
    _JunkSubjects :Str__;
   protected
     procedure SetUseTLS(AValue: TIdUseTLS);  override;
     function GetHandler: TIdIOHandler;
   public
     constructor Create (aOwner :TComponent); reintroduce;
     destructor Destroy; override;
     procedure Connect; override;
     function GetEmail (i :integer) :Email__;
     property Count :integer read _Count;
     property JunkSubjects :Str__ read _JunkSubjects;
   end;

implementation
uses
  SysUtils,
  IdMessageParts;
  ///Log2;



  constructor Email__.Create;
  begin
    inherited Create;
    OnCreateAttachment := CreateAttEvent;
    AttachmentTempDirectory := 'E:\MAIL';
  end;

  destructor Email__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure Email__.CreateAttEvent(const AMsg: TIdMessage; const AHeaders: TStrings; var AAttachment: TIdAttachment);
  begin
    //AAttachment := TidAttachmentFile.Create(AMsg.MessageParts, 'test1.txt');
    AAttachment := TIdAttachmentMemory.Create(AMsg.MessageParts);
  end;

  function Email__.Delete :boolean;
  begin
    if _Server.Delete (_Index) then
      Result := true
    else
      Result := false;
    Sleep (200);
  end;

  function Email__.IsIrrelevant :boolean;
  begin
    Assert (_Server <> nil, 'Email__.IsIrrelevant');
    if SubjectHasOneOf (_Server.JunkSubjects) then
      Result := true
    else
      Result := false
  end;

  function Email__.SubjectHasOneOf (Subjects: TStrings) :boolean;
  var
    i :integer;
  begin
    Result := false;
    for i := 0 to Pred(Subjects.Count) do
      if Str__.IsIn (Subjects[i], Subject) then begin
        Result := true;
        Break;
        end;
  end;

  function Email__.get_File (const aFileName :string) :TIDAttachment;
  var
    i    :integer;
    Part :TIdMessagePart;
    Att  :TIdAttachment;
  begin
    Result := nil;
    for i := 0 to Pred(MessageParts.Count) do begin
      Part := MessageParts[i];
      if (Part is TIdAttachment) then begin
        Att := TIdAttachment(Part);
        if SameText (Att.FileName, aFileName) then begin
          Result := Att;
          Break;
          end;
        end;
      end;
  end;

  function Email__.get_Text: TIDAttachment;
  begin
    if (_Text = nil) then
      _Text := get_File ('bugreport.txt');
    Result := _Text;
  end;

  function Email__.IsBugReport :boolean;
  begin
    Result := false;
    _Image := nil;
    if (Text <> nil) then begin
      _Image := get_File ('screenshot.png');
      Result := true;
      end;
  end;

  procedure Email__.DoLog (aText :string);
  begin
    ///Logger.Info ('%s %s %s', [aText, FormatDateTime ('yymmdd hh:mm', Date), Subject]);
    WriteLn (Format('%s %s %s', [aText, FormatDateTime ('yymmdd hh:mm', Date), Subject]));
  end;

  function Email__.Save :boolean;
  begin
    Result := false;
   end;

  function Email__.get_Report :TBugReport;
  begin
    if (_Report = nil) then
      if (_Text <> nil) then begin
        _Report := TBugReport.Create;
        if (_Text <> nil) then
          _Text.SaveToStream(_Report.Text);
        if (_Image <> nil) then
          _Image.SaveToStream(_Report.Image);
      end;
    Result := _Report;
  end;



  function Emails__.GetItem (i :integer) :Email__;
  begin
    Result := Email__(Get (i))
  end;



  constructor MailServer__.Create (aOwner :TComponent);
  begin
    inherited Create (aOwner);
    _JunkSubjects := Str__.Create;
    _Emails := Emails__.Create;
  end;

  destructor MailServer__.Destroy;
  begin
    if Connected then
      DisconnectNotifyPeer;
    _JunkSubjects.Free;
    _Ssl.Free;
    _Emails.Free;
    inherited Destroy;
  end;

  function MailServer__.GetHandler :TIdIOHandler;
  begin
    ///_Ssl := TCertSSLIOHandler.Create(nil);
    _Ssl := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
    _Ssl.Destination := Host + ':' + IntToStr(_Ssl.Port);
    _Ssl.SSLOptions.SSLVersions := [sslvTLSv1_2];
    Result := _Ssl;
  end;

  procedure MailServer__.SetUseTLS(AValue: TIdUseTLS);
  begin
    if (aValue = utUseImplicitTLS) then
    if (_Ssl = nil) then
      IOHandler := GetHandler;
    inherited SetUseTLS (AValue);
  end;

  procedure MailServer__.Connect;
  begin
    _Emails.Clear;
    UseTLS := utUseImplicitTLS;
    inherited Connect;
    if not Connected then begin
      _Count := 0;
      ///Logger.Warn ('not connected');
      WriteLn ('not connected');
      end
    else begin
      _Count := CheckMessages;
      _Emails.Capacity := _Count;
      //Logger.Info ('new emails: %d', [_Count]);
      end;
  end;

  function MailServer__.GetEmail (i :integer) :Email__;
  begin
    if (i < _Emails.Count) then
      Result := _Emails[i]
    else begin
      Result := Email__.Create;
      Result._Server := Self;
      Result._Index := i;
      Retrieve (Succ(i), Result);  //1..
      Result.ProcessHeaders;
      _Emails.Add (Result);
    end;
  end;

initialization
finalization
end.