unit Events00;
interface
type
  StateChange_ = (Started_, Changed_, Important_, Finished_);
  StateEvent_ = function (aState :StateChange_) :boolean of object;
  StringProc_ = procedure (aValue :string) of object;
  IntegerEvent_ = procedure (aValue :integer) of object;
  BooleanEvent_ = function :boolean of object;
  StringEvent_ = function (aValue :string) :boolean of object;

  StateChange__ = class (TObject)
   private
    FHandler :StateEvent_;
    procedure SetStateChange (aValue :StateChange_);
   protected
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    property Handler :StateEvent_ write FHandler;
    property Change :StateChange_ write SetStateChange;
    end;


implementation


  constructor StateChange__.Create;
  begin
    inherited Create;
  end;

  destructor StateChange__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure StateChange__.SetStateChange (aValue :StateChange_);
  begin
    if Assigned (FHandler) then
      if FHandler (aValue) then
        ;
  end;

end.



