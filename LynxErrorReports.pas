unit LynxErrorReports;
interface
uses
  System.Classes,
  FireDAC.Stan.Intf,
  FireDAC.Comp.Client;

type
  TLynxErrorReports = class(TFDConnection)
  private
   class var _Def: IFDStanConnectionDef;
   class destructor Destroy;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Connect: Boolean;
    function  CreateBlobTable: Boolean;
    function CreateUniqueConstraint: Boolean;
    function Import (AText, AImage :TStream; ADate: TDateTime): Boolean;
  end;

implementation
uses
  FireDAC.Phys.MSSQLDef,
  FireDAC.Phys.MSSQL,
  //FireDAC.Phys.MSSQLMeta,
  FireDAC.Moni.RemoteClient,
  FireDac.Stan.Def,
  FireDac.Stan.Async,

  System.SysUtils,
  Data.DB,
  FireDAC.Stan.Consts,
  FireDAC.Stan.Param,
  Bugreport1;



constructor TLynxErrorReports.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LoginPrompt := False;
end;

destructor TLynxErrorReports.Destroy;
begin
  inherited Destroy;
end;

function TLynxErrorReports.Connect: Boolean;
begin
  Result := False;
  if Connected then
    Result := True
  else begin
    if (_Def = nil) then begin
    // Adding connection definition.
      _Def := FDManager.ConnectionDefs.AddConnectionDef;
      // Set connection definition parameters using IFDStanConnectionDef properties.
      _Def.Name := 'LynxErrorReports';
      _Def.Params.DriverID := 'MSSQL';
      _Def.Params.Values ['CharacterSet'] := 'UTF8';
      _Def.AsString[S_FD_ConnParam_Common_Server] := 'sw4rk4cofi.database.windows.net';
      //_Def.AsInteger[S_FD_ConnParam_Common_Port] := StrToInt(FProperties.edtPort.Text);
      _Def.Params.Database := 'LynxErrorReports';
      _Def.Params.UserName := 'adato';
      _Def.Params.Password := 'L0ngj0hn';
      _Def.Params.MonitorBy := mbRemote;
      // Mark definition persistent and save it into the file
      //_Def.MarkPersistent;
      _Def.Apply;
    end;
    ConnectionDefName := _Def.Name;
    Connected := True;
    if Connected then
      Result := True;
   end;
end;

function TLynxErrorReports.CreateBlobTable: Boolean;
var
  res: LongInt;
begin
  res := ExecSQL('drop table if exists UserReports');
  res := ExecSQL('create table UserReports(' +
   'ID int identity not null,' +
   '[Hash] int not null,' +
   'AppName varchar(16) not null,' +
   'AppVersion char(16) not null,' +
   'Occured datetime not null,' +
   'Category varchar(40),' +
   '[Owner] varchar(50),' +
   'UserName varchar(50),' +
   '[Message] varchar(100),' +
   '[Stack] varchar(500),' +
   'Bugreport varchar(max),' +
   'Screenshot varbinary(max)),' +
   'ExcNumber int)'
   );
  Result := CreateUniqueConstraint;
end;

function TLynxErrorReports.CreateUniqueConstraint: Boolean;
var
  res: LongInt;
begin
  res := ExecSQL('CREATE UNIQUE INDEX [UniqueReport] ON [UserReports]([AppName],[AppVersion],[Occured])');
  Result := (res = 0);
end;

class destructor TLynxErrorReports.Destroy;
begin
  _Def := nil;
end;

function TLynxErrorReports.Import (AText, AImage :TStream; ADate: TDateTime): Boolean;
const
  ImageFile = 'screenshot.png';
var
  params: TFDParams;
  pHash: TFDParam;
  pAppName: TFDParam;
  pAppVersion: TFDParam;
  pOccured: TFDParam;
  pOwner: TFDParam;
  pCategory: TFDParam;
  pUserName: TFDParam;
  pMessage: TFDParam;
  pStack: TFDParam;
  pText: TFDParam;
  pi: TFDParam;
  pExcNumber: TFDParam;
  recs: LongInt;
  bug: TBugReport;
begin
  Result := False;
  AText.Position := 0;
  if (AImage <> nil) then
    AImage.Position := 0;

  bug := TBugReport.Create;
  bug.LoadFromStream(AText);

  params := TFDParams.Create;

  pHash := params.Add;
  pHash.AsInteger := Bug.Hash;

  pAppName := params.Add;
  pAppName.AsString := bug.Executable;

  pAppVersion := params.Add;
  pAppVersion.AsString := bug.Version;

  pOccured := params.Add;
  pOccured.AsDateTime := ADate; //using emil date instead of bug.Exe.Error.Value to avoid time zone

  pCategory := params.Add;
  pCategory.AsString := bug.ExcClass;

  pOwner := params.Add;
  pOwner.AsString := bug.Owner;

  pUserName := params.Add;
  pUserName.AsString := bug.User;

  pMessage := params.Add;
  pMessage.AsString := bug.ExcMessage;

  pStack := params.Add;
  pStack.AsString := bug.Unique;

  pText := params.Add;
  pText.SetStream(AText, ftMemo);

  pi := params.Add;
  if (AImage = nil) then
    pi.DataType := ftGraphic
  else
    pi.SetStream(AImage, ftGraphic);

  pExcNumber := params.Add;
  pExcNumber.AsInteger := bug.ExcNumber;

  bug.Free;
  try
    recs := ExecSQL('insert into UserReports (' +
     'Hash, AppName, AppVersion, Occured, Category, [Owner], UserName, [Message], [Stack], Bugreport, Screenshot, ExcNumber) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', params);
    if recs = 1 then
      Result := True;
  except
    on e: Exception do
      WriteLn(e.Message);
  end;
end;


end.

