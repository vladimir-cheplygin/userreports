unit Tk00;
{$H+}
interface
uses
  Char0,
  Iter0;
type
  LineIndex_ = integer;
  LineLen_ = integer;

  Range_ = record
    Start  :LineIndex_;
    Finish :LineIndex_;
    end;

  Seps__ = class (TObject)
    Sep    :string;
    Seps   :Chars_;
    procedure Assign (const aValue :Seps__);
    procedure SetAsStr (const aValue :string);
    procedure SetAsSet (const aValue :Chars_);
    procedure SetAsComplement (const aValue :Seps__);
    end;

  AbsTokens__ = class (Iterator__)
   private
    FCurr        :Range_;
    procedure SetCurr (aValue :Range_);
   private
   _Trimming     :boolean;
   private
    _Old         :Seps__;
    _New         :Seps__;
    _Incl        :boolean;
    procedure UpdateNew;
    procedure SetIncl (aValue : boolean);
   private
    FLast        :Range_;
    SepFirst     :LineIndex_;
    FLine        :string;
    //_Sep         :string;
    //_Seps        :Chars_;
    _Count       :cardinal;
    _CountLevel  :integer;
    _Backwards   :boolean;
    function GetTotalCount :integer;
    procedure SetCounting (aValue :boolean);
    function GetCounting :boolean;
    procedure SetLine (aLine :string);
   protected
    function GetRestLen :integer;
      virtual;
    function IsInSep (aPos :integer) :boolean;
    procedure SetBackwards (aValue :boolean);
      virtual;
    property Counting :boolean read GetCounting write SetCounting;
    procedure CountToken;
    function GetSeparator :string;
    procedure SetSeparator (aValue :string);
      virtual;
    procedure SetSepSet (aValue :CharSet_);
    procedure SetLineCustom (aLine :string);
      virtual;
    function GetLine :string;
      virtual;
    function GetTokenByNum (aNumber :integer) :string;
      virtual;
    function MOV (Find :boolean) :integer;
      virtual;
    function GetRange (var aRange :Range_) :boolean;
      virtual;
    function GetLeftSep :string;
      virtual;
    function GetRightSep :string;
    function GetToken :string;
      virtual;
    procedure RewindCustom;
      override;
    function NextCustom :boolean;
      override;
    procedure ExtendRight;
    procedure ExtendLeft;
    function GetBase62Number :cardinal;
      virtual;
    function GetFloatNumber :double;
      virtual;
   public
    constructor Create;
      override;
    constructor CreateInc (aSep :string);
      overload;
    constructor CreateInc (aSep :CharSet_);
      overload;
    constructor CreateExc (aSep :string);
      overload;
    constructor CreateExc (aSep :CharSet_);
      overload;
    destructor Destroy;
      override;
    function CopyLine (Start :LineIndex_; Len :LineLen_) :string;
      virtual;
    property Line :string read GetLine write SetLine;
    property Trimming :boolean read _Trimming write _Trimming;
    property Token :string read GetToken;
    property LeftSep :string read GetLeftSep;
    property RightSep :string read GetRightSep;
    function Cha (i :LineIndex_) :Char;         {1..}
    function LEN :integer;
    property Start :LineIndex_ read FCurr.Start;
    property Finish :LineIndex_ read FCurr.Finish;
    property Curr :Range_ read FCurr write SetCurr;
    function NextToken :string;
    function NextSep :boolean;
    function Number :integer;
      virtual;
    function HexNumber :integer;
    function NextNumber :integer;
    function NextFloat :double;
    property Separator :string read GetSeparator write SetSeparator;
    property SepSet :CharSet_ write SetSepSet;
    property Inclusive :boolean read _Incl write SetIncl;
    property TokenByNum[Idx :integer] :string read GetTokenByNum;
    property Count :cardinal read _Count;
    property TotalCount :integer read GetTotalCount;
    function MaxTokenLen :integer;
    property Backwards :boolean read _Backwards write SetBackwards;
    function GetRest :string;
    property Base62Number :cardinal read GetBase62Number;
    property FloatNumber :double read GetFloatNumber;
    end;

 Tokens__ = class (AbsTokens__)
  private
   FNonBlank    :boolean;
  protected
    function GetTokenByNum (aNumber :integer) :string;
      override;
   function MOV (Find :boolean) :integer;
     override;
   function NextCustom :boolean;
     override;
  public
   constructor Create;
     override;
   destructor Destroy;
     override;
    function CopyLine (aStart :LineIndex_; aLen :LineLen_) :string;
      override;
   property NonBlank :boolean read FNonBlank write FNonBlank;
   end;

 LeadingTokens__ = class (Tokens__)
  private
  protected
   function NextCustom :boolean;
     override;
  public
   constructor Create;
     override;
   destructor Destroy;
     override;
   end;

 SingleTokens__ = class (Tokens__)
  private
  protected
   procedure SetLineCustom (aLine :string);
     override;
   function MOV (Find :boolean) :integer;
     override;
   {function NextCustom :boolean;
     override;}
   function GetToken :string;
     override;
   procedure RewindCustom;
     override;
   function GetRestLen :integer;
     override;
  public
   constructor Create;
     override;
   destructor Destroy;
     override;
   end;

 PairTokens__ = class (SingleTokens__)
  private
   _Actual  :array[1..2] of Char;
   s        :byte;
   procedure UpdateActual;
  private
   FInside     :boolean;
   FInsideOnly :boolean;
   FPrevToken  :string;
   FCnt        :integer;
   _Quotes     :boolean;
   function GetInside :boolean;
   function NextPos (SepCh :Char; aSource :string; aStart :integer) :integer;
  protected
   procedure SetBackwards (aValue :boolean);
     override;
   procedure SetSeparator (aValue :string);
     override;
   function MOV (Find :boolean) :integer;
     override;
   function NextCustom :boolean;
     override;
   procedure RewindCustom;
     override;
   function GetLeftSep :string;
     override;
  public
   constructor Create;
     override;
   constructor CreateInc (aSep :Quotes_);
   destructor Destroy;
     override;
   property Inside :boolean read GetInside;
   property InsideOnly :boolean read FInsideOnly write FInsideOnly;
   property Quotes :boolean write _Quotes;
   end;

 StringTokens__ = class (Tokens__)
  private
  protected
   function MOV (Find :boolean) :integer;
     override;
  public
   constructor Create;
     override;
   destructor Destroy;
     override;
   end;

 NewTokens__ = class (Tokens__)
  private
  protected
   function MOV (Find :boolean) :integer;
     override;
  public
   constructor Create;
     override;
   destructor Destroy;
     override;
   end;


implementation
uses
  SysUtils,
  Math,
  Event0,
  a0,
  String0,
  Encode0;
const
  Dummy :string[1] = #1;

  procedure Seps__.Assign (const aValue :Seps__);
  begin
    Sep := aValue.Sep;
    Seps := aValue.Seps;
  end;

  procedure Seps__.SetAsStr (const aValue :string);
  var
    i   :SmallInt;
  begin
    Sep := aValue;
    Seps := [];
    for i := 1 to Length (Sep) do
     {$ifdef VER150}
      Include (Seps, Sep[i]);
     {$else}
      Seps := Seps + [Sep[i]];
     {$endif}
  end;

  procedure Seps__.SetAsSet (const aValue :Chars_);
  var
    c   :Char;
    Len :word;
    i   :word;
  begin
    Len := 0;
    Seps := aValue;
    for c := Low (Char) to High (Char) do
      if (c in Seps) then
        Inc (Len);
    SetLength (Sep, Len);
    i := 0;
    for c := Low (Char) to High (Char) do
      if (c in Seps) then begin
        Inc (i);
        Sep[i] := c;
      end;
  end;

  procedure Seps__.SetAsComplement (const aValue :Seps__);
  var
    Len :word;
    i   :word;
    c   :Char;
  begin
    Len := 0;
    Seps := [];
    for c := Low (Char) to High (Char) do
      if not (c in aValue.Seps) then begin
       {$ifdef VER150}
        Include (Seps, c);
       {$else}
        Seps := Seps + [c];
       {$endif}
        Inc (Len);
        end;
    SetLength (Sep, Len);
    i := 0;
    for c := Low (Char) to High (Char) do
      if (c in Seps) then begin
        Inc (i);
        Sep[i] := c;
        end;
  end;



  constructor AbsTokens__.Create;
  begin
    inherited Create;
    //Valid := true;
    _Incl := true;
    _Old := Seps__.Create;
    _New := Seps__.Create;
    Separator := SpaceTab;
  end;

  destructor AbsTokens__.Destroy;
  begin
    _Old.Free;
    _New.Free;
    inherited Destroy;
  end;

  constructor AbsTokens__.CreateInc (aSep :string);
  begin
    Create;
    _Incl := true;
    Separator := aSep;
  end;

  constructor AbsTokens__.CreateInc (aSep :CharSet_);
  begin
    Create;
    _Incl := true;
    SepSet := aSep;
  end;

  constructor AbsTokens__.CreateExc (aSep :string);
  begin
    Create;
    _Incl := false;
    Separator := aSep;
  end;

  constructor AbsTokens__.CreateExc (aSep :CharSet_);
  begin
    Create;
    _Incl := false;
    SepSet := aSep;
  end;

  procedure AbsTokens__.SetCurr (aValue :Range_);
  begin
    FCurr := aValue;
    FLast := FCurr;
  end;

  procedure AbsTokens__.UpdateNew;
  begin
    if _Incl then
      _New.Assign (_Old)
    else
      _New.SetAsComplement (_Old);
    Valid := (_Old.Sep <> '');
    Rewind;
  end;

  function AbsTokens__.GetSeparator :string;
  begin
    Result := _Old.Sep;
  end;

  procedure AbsTokens__.SetSeparator (aValue :string);
  begin
    _Old.SetAsStr (aValue);
    UpdateNew;
  end;

  procedure AbsTokens__.SetSepSet (aValue :CharSet_);
  begin
    _Old.SetAsSet (aValue);
    UpdateNew;
  end;

  procedure AbsTokens__.SetIncl (aValue : boolean);
  begin
    if (aValue <> _Incl) then begin
      _Incl := aValue;
      UpdateNew;
      end;
  end;

  function AbsTokens__.IsInSep (aPos :integer) :boolean;
  var
    c :Char;
  begin
    if (aPos <= 0) then
      Result := true
    else if (aPos > Length (Fline)) then
      Result := true
    else begin
      c := FLine[aPos];
      if (c in _New.Seps) then
        Result := true
      else
        Result := false;
      end;
  end;

  procedure AbsTokens__.RewindCustom;
  begin
    inherited RewindCustom;
    FCurr.Finish := 0;
    FLast.Finish := 0;
    _Count := 0;
    _CountLevel := 0;
  end;

  procedure AbsTokens__.SetCounting (aValue :boolean);
  begin
    if aValue then
      Inc (_CountLevel)
    else
      Dec (_CountLevel);
  end;

  function AbsTokens__.GetCounting :boolean;
  begin
    Result := (_CountLevel >= 0);
  end;

  procedure AbsTokens__.CountToken;
  begin
    if Counting then
      Inc (_Count);
  end;

  function AbsTokens__.CopyLine (Start :LineIndex_; Len :LineLen_) :string;
  begin
    Result := System.Copy (FLine, Start, Len);
  end;

  function AbsTokens__.GetLine :string;
  begin
    Result := CopyLine (1, Length (FLine));
  end;

  procedure AbsTokens__.SetLineCustom (aLine :string);
  begin
    FLine := aLine;
    if Backwards then
      Str__.Reverse (FLine);
  end;

  procedure AbsTokens__.SetLine (aLine :string);
  begin
    Rewind;
    FCurr.Finish := 0;     //immediate
    FLast.Finish := 0;     //
    SetLineCustom (aLine);
  end;

  procedure AbsTokens__.ExtendRight;
  begin
    Inc (FLast.Finish);
  end;

  procedure AbsTokens__.ExtendLeft;
  begin
    Dec (FLast.Start);
  end;

  function AbsTokens__.MOV (Find :boolean) :integer;
  begin
    Result := Start + 1;
  end;

  function AbsTokens__.LEN :integer;
  begin
    Result := FLast.Finish - FLast.Start;
    {if (Result < 0) then
      Result := 0;}
  end;

  function AbsTokens__.GetRange (var aRange :Range_) :boolean;
  begin
    Result := true;
    aRange.Start := MOV (false);
    aRange.Finish := MOV (true);
  end;

  function AbsTokens__.NextCustom :boolean;
  begin
    Result := false;
    SepFirst := FCurr.Finish;
    FCurr.Start := Succ (FCurr.Finish);
    if (FCurr.Start <= System.Length (FLine)) then
      if not GetRange (FCurr) then begin
        FCurr.Start := Succ (System.Length (FLine));
        FCurr.Finish := FCurr.Start;
        end;
    FLast := FCurr;
    if (LEN < 0) then
      Skip
    else if (FLast.Start > Length (FLine)) then
      Skip
    else begin
      CountToken;
      Result := true;
      end;
  end;

  function AbsTokens__.Cha (i :LineIndex_) :Char;         {1..}
  begin
    Result := Str__.Ch (Line, FLast.Start + i - 1);
  end;

  function AbsTokens__.GetToken :string;
  begin
    Result := CopyLine (FLast.Start, LEN);
    if _Trimming then
      Result := SysUtils.Trim (Result);
  end;

  function AbsTokens__.NextToken :string;
  begin
    Result := '';
    if Next then
      Result := Token;
  end;

  function AbsTokens__.NextSep :boolean;
  var
    s :string;
  begin
    Result := false;
    if Next then begin
      s := RightSep;
      if (s <> '') then
        if Str__.IsIn (s[1], Separator) then
          Result := true;
      end;
  end;

  function AbsTokens__.Number :integer;
  var
    Res      :integer;
    NumStr   :string;
  begin
    Result := 0;
    NumStr := Str__.Filtered (Token, ['+','-','0'..'9']{NumericSet});
    if Str__.Val (Res, NumStr) then
      Result := Res;
  end;

  function AbsTokens__.HexNumber :integer;
  begin
    try
      Result := StrToInt ('$' + Token);
    except
      Result := 0;
      end;
  end;

  function AbsTokens__.GetFloatNumber :double;
  begin
    if (LEN <= 0) then
      Result := 0
    else
      Result := StrToFloat (Token, StdSettings);
  end;

  function AbsTokens__.GetBase62Number :cardinal;
  begin
    Result := __Encoder.Decode (Token);
  end;

  function AbsTokens__.NextNumber :integer;
  begin
    Result := 0;
    if Next then
      Result := Number;
  end;

  function AbsTokens__.NextFloat :double;
  begin
    Result := 0;
    if Next then
      Result := FloatNumber;
  end;

  function AbsTokens__.GetLeftSep :string;
  begin
    if (SepFirst = 0) then
      SepFirst := 1;
    Result := CopyLine (SepFirst, Start - SepFirst);
  end;

  function AbsTokens__.GetRightSep :string;
  var
    s   :LineIndex_;
  begin
    s := Str__.Skip (_New.Sep, FLine, FCurr.Finish);
    Result := CopyLine (FCurr.Finish, s - FCurr.Finish);
  end;

  function AbsTokens__.GetRestLen :integer;
  begin
    Result := System.Length (FLine) - FCurr.Finish + 1
  end;

  function AbsTokens__.GetRest :string;
  begin
    Result := CopyLine (FCurr.Finish, GetRestLen)
  end;

  function AbsTokens__.GetTokenByNum (aNumber :integer) :string;
  begin
    if (aNumber = 0) then
      Result := LeftSep
    else begin
      Rewind;
      while (aNumber > 0) and Next do   {skip aNumber-1 Tokens}
        Dec (aNumber);
      Result := Token;
      end;
  end;

  function AbsTokens__.MaxTokenLen :integer;
  begin
    Result := 0;
    Rewind;
    while Next do
      Result := Max (Result, LEN);
  end;

  function AbsTokens__.GetTotalCount :integer;
  begin
    Rewind;
    while Next do
      ;
    Result := Count;
    Rewind;
  end;

  procedure AbsTokens__.SetBackwards (aValue :boolean);
  begin
    if (aValue <> _Backwards) then begin
      _Backwards := aValue;
      Str__.Reverse (FLine);
      Rewind;
      end;
  end;




  constructor Tokens__.Create;
  begin
    inherited Create;
  end;

  destructor Tokens__.Destroy;
  begin
    inherited Destroy;
  end;

  function Tokens__.CopyLine (aStart :LineIndex_; aLen :LineLen_) :string;
  begin
    Result := inherited CopyLine (aStart, aLen);
    if Backwards then
      Str__.Reverse (Result);
  end;

  function Tokens__.MOV (Find :boolean) :integer;
  begin
    if Find then
      Result := Str__.Locate (_New.Sep, FLine, Start)
    else
      Result := Str__.Skip (_New.Sep, FLine, Start);
  end;

  function Tokens__.NextCustom :boolean;
  begin
    Result := false;
    Counting := false;
    while not Result and inherited NextCustom do
      if FNonBlank and Str__.IsBlank (Token) then
        {skip}
      else
        Result := true;
    Counting := true;
    if Result then
      CountToken;
  end;

  function Tokens__.GetTokenByNum (aNumber :integer) :string;
  var
    Save :boolean;
  begin
    Save := Backwards;
    if (aNumber > 0) then
      Backwards := false
    else begin
      aNumber := -aNumber;
      Backwards := true;
      end;
    Result := inherited GetTokenByNum (aNumber);
    Backwards := Save;
  end;




  constructor SingleTokens__.Create;
  begin
    inherited Create;
  end;

  destructor SingleTokens__.Destroy;
  begin
    inherited Destroy;
  end;

  function SingleTokens__.GetRestLen :integer;
  begin
    Result := inherited GetRestLen;
    if (Result > 0) then
    if (Fline[Length(Fline)] = Dummy) then
      Dec (Result);
  end;

  procedure SingleTokens__.RewindCustom;
  begin
    inherited RewindCustom;
    NonBlank := false;
  end;

  procedure SingleTokens__.SetLineCustom (aLine :string);
  begin
    inherited SetLineCustom (aLine);
    if IsInSep (1) then
      System.INSERT (Dummy, FLine, 1);
    if IsInSep (Length (FLine)) then
      FLine := FLine + Dummy;
  end;

  function SingleTokens__.MOV (Find :boolean) :integer;
  begin
    if Find then
      //Result := Str__.Locate (_Sep, FLine, Start)
      Result := Str__.Next (_New.Seps, FLine, Start, Length(FLine))
    else
      if _Incl  then
        Result := Start
      else
        Result := Str__.Skip (_New.Sep, FLine, Start);
  end;

  function SingleTokens__.GetToken :string;
  begin
    Result := inherited GetToken;
    if (Result = Dummy) then
      Result := '';
  end;




  constructor LeadingTokens__.Create;
  begin
    inherited Create;
  end;

  destructor LeadingTokens__.Destroy;
  begin
    inherited Destroy;
  end;

  function LeadingTokens__.NextCustom :boolean;
  {tok/TOK/TOK = filters start off}
  begin
    Result := false;
    Counting := false;
    while not Result and inherited NextCustom do
      if (Length (LeftSep) > 0) then
        Result := true;
    Counting := true;
    if Result then
      CountToken;
  end;




  constructor PairTokens__.Create;
  begin
    inherited Create;
    Separator := '""';           //default
    InsideOnly := true;
    Quotes := true;
  end;

  constructor PairTokens__.CreateInc (aSep :Quotes_);
  begin
    inherited CreateInc ([aSep.Open, aSep.Close]);
    UpdateActual;
  end;

  destructor PairTokens__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure PairTokens__.UpdateActual;
  begin
    if Backwards then begin
      _Actual[1] := _New.Sep[2];
      _Actual[2] := _New.Sep[1];
      end
    else begin
      _Actual[1] := _New.Sep[1];
      _Actual[2] := _New.Sep[2];
      end;
  end;

  procedure PairTokens__.SetBackwards (aValue :boolean);
  begin
    inherited SetBackwards (aValue);
    UpdateActual;
  end;

  procedure PairTokens__.SetSeparator (aValue :string);
  begin
    if (Length (aValue) = 1) then
      aValue := aValue + aValue;
    inherited SetSeparator (aValue);
    UpdateActual
  end;

  procedure PairTokens__.RewindCustom;
  begin
    inherited RewindCustom;
    s := 1;
    FInside := true;
    FPrevToken := '';
    FCnt := 0;
  end;

  function PairTokens__.NextPos (SepCh :Char; aSource :string; aStart :integer) :integer;
  var
    Found    :boolean;
    Fini     :integer;
    Ch       :Char;
    OpenCh   :Char;
    CloseCh  :Char;
  begin
    Found := false;
    Fini := Length (aSource);
    OpenCh := _Actual[1];
    CloseCh := _Actual[2];
    while not Found and (aStart <= Fini) do begin
      Ch := aSource[aStart];
      if (SepCh = OpenCh) or (SepCh = CloseCh) then begin
        if (Ch = OpenCh) and (s = 1) then
          Inc (FCnt)
        else if (Ch = CloseCh) and (s = 2) then
          Dec (FCnt);
        if (Ch = SepCh) then begin
          if (s = 1) then
            Found := true
          else if (FCnt = 0) then
            Found := true;
          end
        else if _Quotes and (Ch in [SingleQuote, DoubleQuote]) then
          aStart := NextPos (Ch, aSource, aStart + 1);
        if not Found then
          Inc (aStart);
        end
      else begin
        if (Ch = SepCh) then
          Found := true
        else begin       {not found}
          if not _Quotes then
            Inc (aStart)
          else begin
            if (Ch in [SingleQuote,DoubleQuote]) then
              aStart := NextPos (Ch, aSource, aStart + 1)
            else begin
              if (not FInside {not upd yet}) then
                if ((OpenCh = DoubleQuote) and (Ch = SingleQuote))
                or ((OpenCh = SingleQuote) and (Ch = DoubleQuote))
               then repeat
                  if (aStart < Fini) then begin
                    Inc (aStart);
                    Ch := aSource[aStart];
                    end;
                  until (Ch = SingleQuote) or (aStart >= Fini);
              Inc (aStart);
              end;
            end;
          end;
        end;
      end;
    Result := aStart;
  end;

  function PairTokens__.MOV (Find :boolean) :integer;
  begin
    if Find then begin
      Result := NextPos (_Actual[s], FLine, Start);
      if (s = 1) then
        s := 2
      else
        s := 1;
      end
    else
      Result := Start;
  end;

  function PairTokens__.GetInside :boolean;
  begin
    Assert (not InsideOnly, 'senseless');
    Result := FInside;
  end;

  function PairTokens__.GetLeftSep :string;
  begin
    if InsideOnly then
      Result := FPrevToken
    else
      Result := inherited GetLeftSep;
  end;

  function PairTokens__.NextCustom :boolean;
  begin
    Result := false;
    Counting := false;
    while not Result and inherited NextCustom do begin
      FInside := not FInside;
      if FInside then
        Result := true
      else begin
        if InsideOnly then
          FPrevToken := Token
        else
          Result := true;
        end;
      end;
    Counting := true;
    if Result then
      CountToken;
  end;

  


  constructor StringTokens__.Create;
  begin
    inherited Create;
  end;

  destructor StringTokens__.Destroy;
  begin
    inherited Destroy;
  end;

  function StringTokens__.MOV (Find :boolean) :integer;
  begin
    if Find then begin
      Result := Str__.Pos (_New.Sep, FLine, FCurr.Start);
      if (Result = 0) then
        Result := Length (FLine) + 1;
      end
    else begin
      Result := FCurr.Start - 1;             //start has been incremented
      if Result < 1 then
        Result := 1;
      if Str__.IsLocated (_New.Sep, FLine, Result) then
        Inc (Result, Length (_New.Sep));
      end;
  end;




  constructor NewTokens__.Create;
  begin
    inherited;
  end;

  destructor NewTokens__.Destroy;
  begin
    inherited;
  end;

  function NewTokens__.MOV(Find: boolean): integer;
  begin
    if Find then
      Result := Str__.Next (_New.Seps, FLine, Start, Length (FLine))
    else
      Result := Str__.Skip (_New.Sep, FLine, Start);
  end;

initialization
finalization
end.
