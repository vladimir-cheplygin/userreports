unit Tim1;
interface
uses
  Windows,
  SysUtils,
  Events00;
const
  MillisecondsPerSec = 1000;
  SecondsPerMin = 60;
type
  Milliseconds_ = cardinal;
  Seconds_ = cardinal;
  Minutes_ = cardinal;

  SimpleTimeout__ = class (TObject)
   private
    FValue      :Seconds_;
    FStart      :MilliSeconds_;
    FFinish     :MilliSeconds_;
    FActive     :boolean;
    FHasExpired :boolean;
    FWasStarted :boolean;
    FWasStopped :boolean;
    FTicks      :Milliseconds_;

    function GetMinutes :Minutes_;
    procedure SetMinutes (aValue :Minutes_);
    procedure SetAsString (const aValue :string);
    function GetRawRemaining :Milliseconds_;
    function GetTicks :Milliseconds_;
    function GetPassed: Seconds_;
    function GetFinish :Milliseconds_;
    procedure SetStart (aValue :Milliseconds_);
   protected
    property Finish :MilliSeconds_ read GetFinish;
    procedure SetValue (aValue :Seconds_);
    function GetRemaining :Seconds_;
    procedure SetActive (IsOn :boolean);
    function GetActive :boolean;
    procedure DoOnStart;
      virtual;
    procedure DoOnProgress;
      virtual;
    procedure DoOnFinish;
      virtual;
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    function GetAsString :string;
    property HasExpired :boolean read FHasExpired;
    property Seconds :Seconds_ read FValue write SetValue;
    property AsMinutes :Minutes_ read GetMinutes write SetMinutes;
    property Active :boolean read GetActive write SetActive;
    property AsString :string read GetAsString write SetAsString;
    class function GetSecondsAsString (t :Seconds_) :string;
    class function GetSecondsAsStringApprox (t :Seconds_) :string;
    function GetRawPassed :Milliseconds_;
    property Passed :Seconds_ read GetPassed;
    function GetPassedAsString :string;
    property Remaining :Seconds_ read GetRemaining;
    function GetRemainingAsString :string;
    property Ticks :Milliseconds_ read FTicks;
    property Start :Milliseconds_ read FStart write SetStart;
    procedure Delay (aValue :Seconds_);
    end;

  ProgressTimeout__ = class (SimpleTimeout__)
   private
    FOnStart    :IntegerEvent_;
    FOnProgress :IntegerEvent_;
   protected
    procedure DoOnStart;
      override;
    procedure DoOnProgress;
      override;
    procedure DoOnFinish;
      override;
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    property OnStart :IntegerEvent_ write FOnStart;
    property OnProgress :IntegerEvent_ write FOnProgress;
    end;


const
  TimeStrSize = 8;
  DateStrSize = 10;
  TimeDateStrSize = TimeStrSize + 1 + DateStrSize;
type
  TimeStr_ = string[TimeStrSize];
  DateStr_ = string[DateStrSize];
  TimeDateStr_ = string[TimeDateStrSize];

  Interval__ = class (TObject)
   private
    FStartTime      :TDateTime;
    FFinishTime     :TDateTime;
    FActive         :boolean;

    function TheNow :TDateTime;
   protected
    procedure SetActive(const aValue :boolean);
      virtual;
    function GetSeconds :Double;
    procedure SetSeconds (Secs :Double);
   public
    constructor Create;
      //virtual;
    destructor Destroy;
      override;
    function Days :Double;
    function Str :TimeStr_;
    property Seconds :Double read GetSeconds write SetSeconds;
    property Active :boolean read FActive write SetActive;
    end;

   StatInterval__ = class (Interval__)
    private
     FTotalSecs :Double;
     FTotalRecs :longint;
     FCurrRecs  :longint;
     FLeft      :Double;
    protected
    public
     FAve       :Double;

     constructor Create (aTotalRecs :longint);
     procedure Stop;
     function Left :TimeStr_;
     function Total :TimeStr_;
     end;


  function ToDosDate (aDate :ShortString) :longint;
  function FromDosDate (aDate :longint) :ShortString;

  function TimeToDos (const FileTime :TFileTime) :longint;
  function SecToHourStr (Seconds :Double) :ShortString;

implementation
uses
  Forms {Application},
  Char0,
  String0,
  Tk00;


  constructor SimpleTimeout__.Create;
  begin
    inherited Create;
    Seconds := 1;
  end;

  destructor SimpleTimeout__.Destroy;
  begin
    inherited Destroy;
  end;

  function SimpleTimeout__.GetTicks :Milliseconds_;
  begin
    FTicks := Windows.GetTickCount{DWORD};
    Result := FTicks;
  end;

  function SimpleTimeout__.GetFinish :Milliseconds_;
  begin
    if (FFinish = 0) then    //recalc
      FFinish := FStart + Round (FValue * MillisecondsPerSec);
    Result := FFinish;
  end;

  procedure SimpleTimeout__.DoOnStart;
  begin
  end;

  procedure SimpleTimeout__.DoOnProgress;
  begin
  end;

  procedure SimpleTimeout__.DoOnFinish;
  begin
  end;

  procedure SimpleTimeout__.SetStart (aValue :Milliseconds_);
  begin
    FStart := aValue;
    FFinish := 0;                   //will recalc
    FHasExpired := false;
    FWasStopped := false;
    FActive := true;
  end;

  procedure SimpleTimeout__.SetActive (IsOn :boolean);
  begin
    FActive := false;
    if IsOn then begin
      Start := GetTicks;
      FWasStarted := true;
      DoOnStart;
      end
    else if FWasStarted then begin
      FWasStarted := false;
      FWasStopped := true;
      FFinish := GetTicks;   //actual finish
      DoOnFinish;
      end;
  end;

  function SimpleTimeout__.GetActive :boolean;
  begin
    if FActive then begin
      //Application__.Instance.Yield;
      Windows.Sleep (0);
      Application.ProcessMessages;

      if Application.Terminated then
        Active := false
      else if (GetTicks > Finish) then begin
        FHasExpired := true;
        Active := false;
        end
      else
        DoOnProgress;
      end;
    Result := FActive;
  end;

  procedure SimpleTimeout__.SetValue (aValue :Seconds_);
  begin
    FValue := aValue;
    FFinish := 0;
    {Start;}
  end;

  function SimpleTimeout__.GetRawPassed :Milliseconds_;
  begin
    Result := 0;
    if FActive then
      Result := (GetTicks - FStart)
    else if FWasStopped then begin
      if (Finish > FStart) then
        Result := (Finish - FStart);
      end;
  end;

  function SimpleTimeout__.GetRawRemaining :Milliseconds_;
  begin
    Result := 0;
    if FActive then
      Result := (Finish - GetTicks);
  end;

  function SimpleTimeout__.GetRemaining :Seconds_;
  begin
    Result := GetRawRemaining DIV MillisecondsPerSec;
  end;

  function SimpleTimeout__.GetPassed :Seconds_;
  begin
    Result := GetRawPassed DIV MillisecondsPerSec;
  end;

  function SimpleTimeout__.GetMinutes :Minutes_;
  begin
    Result := Round (Seconds / SecondsPerMin);
  end;

  procedure SimpleTimeout__.SetMinutes (aValue :Minutes_);
  begin
    Seconds := aValue * SecondsPerMin;
  end;

  class function SimpleTimeout__.GetSecondsAsString (t :Seconds_) :string;
  var
    d, h, m, s :cardinal;
  begin
    d := System.Trunc (t / SecsPerDay);
    t := t - d * SecsPerDay;
    h := System.Trunc (t / 3600);
    t := t - h * 3600;
    m := System.Trunc (t / 60);
    t := t - m * 60;
    s := System.Trunc (t);
    Result := NumStr__.AsString (s, 2);
    if (m > 0) then
      Result := NumStr__.AsString (m, 2) + Colon + Result;
    if (h > 0) then
      Result := NumStr__.AsString (h, 2) + Colon + Result;
    if (d > 0) then
      Result := NumStr__.AsString (d, 2) + Space + Result;
  end;

  class function SimpleTimeout__.GetSecondsAsStringApprox (t :Seconds_) :string;
  begin
    if (t > SecsPerDay) then
      Result := IntToStr (Round(t / SecsPerDay)) + 'd'
    else if (t > 3600) then
      Result := IntToStr (Round(t / 3600)) + 'h'
    else if (t > 60) then
      Result := IntToStr (Round (t / 60)) + 'm'
    else
      Result := IntToStr (t) + 's'
  end;

  function SimpleTimeout__.GetAsString :string;
  begin
    Result := GetSecondsAsString (Seconds);
  end;

  function SimpleTimeout__.GetPassedAsString :string;
  begin
    Result := GetSecondsAsString (GetPassed);
  end;

  function SimpleTimeout__.GetRemainingAsString :string;
  begin
    Result := GetSecondsAsString (GetRemaining);
  end;

  procedure SimpleTimeout__.SetAsString (const aValue :string);
  var
    t   :Tokens__;
    h   :integer;
    m   :integer;
    s   :integer;
  begin
    h := 0; m := 0; s := 0;
    t := Tokens__.CreateExc ('0123456789');
    t.Backwards := true;
    t.Line := UpperCase (aValue);
    while t.Next do begin
      if (0 <> System.Pos ('S', t.LeftSep)) then
        s := t.NUMBER
      else if (0 <> System.Pos ('M', t.LeftSep)) then
        m := t.NUMBER
      else if (0 <> System.Pos ('H', t.LeftSep)) then
        h := t.NUMBER
      else begin
        if (t.Count = 1) then
          s := t.NUMBER
        else if (t.Count = 2) then
          m := t.NUMBER
        else if (t.Count = 3) then
          h := t.NUMBER;
        end;
      end;
    Seconds := h * 3600 + m * 60 + s;
    t.Destroy;
  end;

  procedure SimpleTimeout__.Delay (aValue :Seconds_);
  begin
    Seconds := aValue;
    Active := true;
    while Active do
     ;
  end;





  constructor ProgressTimeout__.Create;
  begin
    inherited Create;
  end;

  destructor ProgressTimeout__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure ProgressTimeout__.DoOnStart;
  begin
    if Assigned (FOnStart) then
      FOnStart (GetRawRemaining);             {FFinish - FStart (not Active)}
  end;

  procedure ProgressTimeout__.DoOnProgress;
  begin
    if Assigned (FOnProgress) then
      FOnProgress (GetRawPassed);               {not backwards}
  end;

  procedure ProgressTimeout__.DoOnFinish;
  begin
    if Assigned (FOnStart) then
      FOnStart (0);
  end;


  function ToDosDate (aDate :ShortString) :longint;
  begin
    Result := DateTimeToFileDate (StrToDateTime (aDate))
  end;

  function FromDosDate (aDate :longint) :ShortString;
  begin
    Result := DateTimeToStr (FileDateToDateTime (aDate));
  end;

  function TimeToDos (const FileTime :TFileTime) :longint;
  var
    LocalFileTime :TFileTime;
    Res           :LongRec;
  begin
    Result := -1;
    if FileTimeToLocalFileTime (FileTime, LocalFileTime) then
      if FileTimeToDosDateTime (LocalFileTime, Res.Hi, Res.Lo) then
        Result := longint(Res);
  end;

  function SecToHourStr (Seconds :Double) :ShortString;
  var
    t       :double;
    h, m, s :longint;
  begin
    t := Seconds;
    h := System.Trunc (t / 3600);
    t := t - h * 3600;
    m := System.Trunc (t / 60);
    t := t - m * 60;
    s := System.Trunc (t);
    Result := NumStr__.AsString (h, 2) + ':' +
              NumStr__.AsString (m, 2) + ':' +
              NumStr__.AsString (s, 2);
  end;



  constructor Interval__.Create;
  begin
    inherited Create;
    //Start;
  end;

  destructor Interval__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure Interval__.SetActive (const aValue :boolean);
  begin
    if (FActive <> aValue) then begin
      FActive := aValue;
      if FActive then
        FStartTime := SysUtils.Now
      else
        FFinishTime := SysUtils.Now;
      end;
  end;

  function Interval__.TheNow :TDateTime;
  begin
    if Active then
      Result := SysUtils.Now
    else
      Result := FFinishTime;
  end;

  function Interval__.Days :Double;
  begin
    Result := (TheNow - FStartTime);
  end;

  function Interval__.GetSeconds :Double;
  begin
    Result := Days * SysUtils.SecsPerDay;
  end;

  procedure Interval__.SetSeconds (Secs :Double);
  begin
    FFinishTime := SysUtils.Now;
    FStartTime := FFinishTime - (Secs / SysUtils.SecsPerDay);
  end;

  function Interval__.Str :TimeStr_;
  begin
    System.Str (Seconds:3:2, Result);
    {Result := SecToHourStr (Seconds); }
  end;



  constructor StatInterval__.Create (aTotalRecs :longint);
  begin
    inherited Create;
    FTotalRecs := aTotalRecs;
    FCurrRecs := 0;
    FTotalSecs := 0;
  end;

  procedure StatInterval__.Stop;
  var
    s :Double;
  begin
    Active := false;
    INC (FCurrRecs);
    s := Seconds;
    if (FAve <> 0) then                 {filter out peaks}
      if (s > (FAve * 200)) then
        s := FAve;
    FTotalSecs := FTotalSecs + s;
    FAve := FTotalSecs / FCurrRecs;
    FLeft := FAve * (FTotalRecs - FCurrRecs);
  end;

  function StatInterval__.Left :TimeStr_;
  begin
    Result := SecToHourStr (FLeft);
  end;

  function StatInterval__.Total :TimeStr_;
  begin
    Result := SecToHourStr (FTotalSecs);
  end;



end.
