unit Encode0;
interface
type
  CharRange_ = record
    Start  :AnsiChar;
    Finish :AnsiChar;
    Len    :byte;
  end;

  Encoder__ = class (TObject)
   private
    _Ranges  :array [1..3] of CharRange_;
    _Len     :word;
    _Buf     :ShortString;
    _Base    :word;
    procedure AddRange (aStart, aFinish :AnsiChar);
   protected
   public
     constructor Create;
     destructor Destroy;
       override;
     function Encode (aValue :cardinal) :string;
    function Decode (const aValue :Ansistring) :cardinal;
  end;

var
  __Encoder :Encoder__;

implementation
uses
  SysUtils,
  String0;

constructor Encoder__.Create;
begin
  inherited Create;
  AddRange ('0', '9');
  AddRange ('A', 'Z');
  AddRange ('a', 'z');
end;

destructor Encoder__.Destroy;
begin
  inherited Destroy;
end;

procedure Encoder__.AddRange (aStart, aFinish :AnsiChar);
var
  c :AnsiChar;
begin
  Inc (_Len);
  _Ranges[_Len].Start := aStart;
  _Ranges[_Len].Finish := aFinish;
  _Ranges[_Len].Len := Ord (aFinish) - Ord (aStart) + 1;
  for c := aStart to aFinish do
    _Buf := _Buf + c;
  _Base := Length (_Buf);
end;

function Encoder__.Decode (const aValue :Ansistring) :cardinal;
var
  i      :word;
  j      :word;
  Digit  :AnsiChar;
  Value  :word;
  Weight :cardinal;
  Prev   :word;
begin
  Result := 0;
  Weight := 1;
  Value := 0;
  for i := Length (aValue) downto 1 do begin
    Digit := aValue[i];
    Prev := 0;
    for j := 1 to 3 do begin
      if (Digit <= _Ranges[j].Finish) then begin
        Value := Ord (Digit) - Ord (_Ranges[j].Start) + Prev;
        Break;
        end
      else
        Inc (Prev, _Ranges[j].Len);
      end;
    Result := Result + Weight * Value;
    if (i > 1) then                     //avoid overflow for cardinal
      Weight := Weight * _Base;
    end;
end;

function Encoder__.Encode (aValue :cardinal) :string;
var
  Value :word;
  Digit :AnsiChar;
begin
  Result := '';
  repeat
    Value := aValue Mod _Base;
    Digit := _Buf[1 + Value];
    System.Insert (Digit, Result, 1);
    aValue := aValue Div _Base;
  until (aValue = 0);
end;


initialization
  __Encoder := Encoder__.Create;
finalization
  FreeAndNil (__Encoder);
end.
