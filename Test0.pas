unit Test0;
interface

  function Test: Boolean;

implementation
uses
  System.Classes,
  LynxErrorReports;


  function Test: Boolean;
  var
    DB: TLynxErrorReports;
    bug: TMemoryStream;
    img: TMemoryStream;
  begin
    Result := False;
    bug := TMemoryStream.Create;
    bug.LoadFromFile('bugreport.txt');
    img := TMemoryStream.Create;
    img.LoadFromFile('screenshot.png');
    Db := TLynxErrorReports.Create(nil);
    if Db.Connect then begin
      if Db.Import(bug, img) then
        Result := True;
    end;
    Db.Free;
    bug.Free;
    img.Free;
  end;


end.