unit Tim0;
interface
uses
  SysUtils, //incmonth
  Tk00;
type
  TTime = type TDateTime;
  TDate = type TDateTime;
const
  Future = 40000;
  YearMonths = 12;
  MonthWeeks = 4;
  MonthDays = 31;
  MinuteSeconds = 60;
  HourMinutes = 60;
  HourSeconds = HourMinutes * MinuteSeconds;
  DayHours = 24;
  DayMinutes = DayHours * HourMinutes;
  DaySeconds = DayMinutes * 60;
  YearQuarters = 4;
  Unk_ = 0; Mon_ = 1; Tue_ = 2; Wed_ = 3; Thu_ = 4; Fri_ = 5; Sat_ = 6; Sun_ = 7;
  WeekDays = Sun_ - Mon_ + 1;
  Jan_ = 1; Feb_ = 2; Mar_ = 3; Apr_ = 4;  May_ = 5;  Jun_ = 6;
  Jul_ = 7; Aug_ = 8; Sep_ = 9; Oct_ = 10; Nov_ = 11; Dec_ = 12;
const
  Factor = 1950;
  UTCMonths: array [Jan_..Dec_] of string =
   ('January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December');
  MyShortMonthNames: array[Jan_..Dec_] of string =
    ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
type
  Seq_ = 1..3;
  YearMonth_ = Jan_..Dec_;
  MonthDay_ = 1..MonthDays;
const
  Year_ = 1; Month_ = 2; Day_ = 3;
type
  Ymd_ = Year_..Day_;
  YmdSet_ = set of Ymd_;
  YmdValue_ = SmallInt;  //was word
  YmdValues_ = array [Ymd_] of YmdValue_;
  WeekDay_ = Unk_..Sun_; //YmdValue_;
  WeekDays_ = set of Mon_..Sun_;
const
  YmdSet :YmdSet_ = [Year_, Month_, Day_];
type
  Date0__ = class (TObject {Published__})
   private
    FSeq    :array [Seq_] of ShortInt;
   protected
    FVal    :YmdValues_;
    function GetValue (aYmd :Ymd_) :YmdValue_;
    procedure SetValue (aYmd :Ymd_; aValue :YmdValue_);
    function GetSeqValue (aSeq :Seq_) :YmdValue_;
    procedure SetSeqValue (aSeq :Seq_; aValue :YmdValue_);
    function GetYear  :YmdValue_;
    procedure SetYear (aValue :YmdValue_);
      virtual;
    function GetMonth  :YmdValue_;
    procedure SetMonth (aValue :YmdValue_);
      virtual;
    function GetDay  :YmdValue_;
    procedure SetDay (aValue :YmdValue_);
      virtual;
   public
    constructor Create;
      virtual;
      //override;
    destructor Destroy;
      override;
    procedure SetSeq (First, Second, Third :Ymd_);
    property Value[Index :Ymd_] :YmdValue_ read GetValue write SetValue;
    property SeqValue[Index :Seq_] :YmdValue_ read GetSeqValue write SetSeqValue;
    property Raw :YmdValues_ read FVal write FVal;
    class function FullYears (Months :YmdValue_) :YmdValue_;
    end;

  DateTime1__ = class (Date0__)
   private
    function GetAsJulian :longint;
    procedure SetAsJulian (Julian :longint);
   private
    FValid  :boolean;
    function GetWeek :YmdValue_;
    {function GetYearWeek :YmdValue_;}
    procedure SetWeek (aValue :YmdValue_);
    function GetCalendarWeek :YmdValue_;
    function GetYearDay :YmdValue_;
   protected
    FHour   :YmdValue_;
    FMin    :YmdValue_;
    FSec    :YmdValue_;
    FMSec   :YmdValue_;
    function DeDate (aDate :TDateTime; var aYear, aMon, aDay :YmdValue_) :boolean;
    function DeTime (aDate :TDateTime; var aHour, aMin, aSec, aMs :YmdValue_) :boolean;
    function GetDate :TDateTime;
    function GetDosDate :longint;
    procedure SetDosDate (aDate :longint);
    procedure SetYear (aYear :YmdValue_);
      override;
    procedure SetMonth (aMonth :YmdValue_);
      override;
    procedure SetDay (aDay :YmdValue_);
      override;
    function GetQuarter :YmdValue_;
    procedure SetQuarter (aValue :YmdValue_);
    function GetWeekDay :WeekDay_;
    procedure SetWeekDay (aWeekDay :WeekDay_);
    procedure SetDate (aDate :TDateTime);
    procedure SetTime (aDate :TTime);
   public
    constructor Create;
      override;
    property DosValue :longint read GetDosDate write SetDosDate;
    property Year :YmdValue_ read GetYear write SetYear;
    property Month :YmdValue_ read GetMonth write SetMonth;
    property Day :YmdValue_ read GetDay write SetDay;
    property Hour :YmdValue_ read FHour write FHour;
    property Minutes :YmdValue_ read FMin write FMin;
    property Seconds :YmdValue_ read FSec write FSec;
    property Quarter :YmdValue_ read GetQuarter write SetQuarter;
    property WeekDay :WeekDay_ read GetWeekDay write SetWeekDay;
    property Week :YmdValue_ read GetWeek write SetWeek;
    property CalendarWeek :YmdValue_ read GetCalendarWeek;
    function Days :YmdValue_;
    property Time :TTime write SetTime;
    property Valid :boolean read FValid;
    property AsJulian :longint read GetAsJulian write SetAsJulian;
    property YearDay :YmdValue_ read GetYearDay;
   published
    property Value :TDateTime read GetDate write SetDate;
    end;

const
  sDate_ = 1; sDateTime_ = 2; sTime_ = 3; sMilli_ = 4;
type
  DateSeps_ = string[sMilli_];
type
  DateTime__ = class (DateTime1__)
   private
    d       :SingleTokens__;
    s       :Tokens__;
    t       :SingleTokens__;
    m       :SingleTokens__;
    FStrMon :boolean;
    FHasMil :boolean;

    function GetWeekDayStr :string;
    procedure SetWeekDayStr (aValue :string);
    procedure SetDateSep (aValue :string);
    procedure SetTimeSep (aValue :string);
    procedure SetDateTimeSep (aValue :string);
    function GetNextNum (var aNum :YmdValue_; var aSource :string) :boolean;
   protected
    function GetTimeStr :String;
    procedure SetTimeStr (aTime :String);

    function GetDateStr :String;
    procedure SetDateStr (aDate :String);

    function GetDateTimeStr :String;
    procedure SetDateTimeStr (aDateTime :String);
    function ImplicitYear :YmdValue_;
      virtual;
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    property WeekDayStr :string read GetWeekDayStr write SetWeekDayStr;
    property TimeStr :string read GetTimeStr write SetTimeStr;
    property DateStr :string read GetDateStr write SetDateStr;
    property AsString :string read GetDateTimeStr write SetDateTimeStr;
    function Parse (aValue :string) :boolean;
    function Rfc822 :string;
    property StrMon :boolean read FStrMon write FStrMon;
    property DateSep :string write SetDateSep;
    property TimeSep :string write SetTimeSep;
    property DateTimeSep :string write SetDateTimeSep;
    end;

const
  DefaultDateSeps :DateSeps_ = '- :.';

  function DayName (Day :WeekDay_) :string;
  function DateTimeEqual (Dt1, Dt2 :TDateTime) :boolean;
  function MinutesDiff (aStart, aFinish :TTime) :integer;
  function ShortMonthToYmdValue (aMonth :string) :YmdValue_;

implementation
uses
  Math,
  DateUtils,
  a0,
  String0;

  function AsIndex (Day :WeekDay_) :ShortInt;  //1..7 -> 0..6
  begin
    if (Day = Sun_) then
      Result := 1
    else
      Result := Succ (Day);
  end;

  function AsDay (Index :ShortInt) :WeekDay_;
  begin
    if (Index = 0) then
      Result := Sun_
    else
      Result := Index;
  end;

  function DayName (Day :WeekDay_) :string;
  begin
    Result := StdSettings.ShortDayNames[AsIndex (Day)];
  end;

  function DateTimeEqual (Dt1, Dt2 :TDateTime) :boolean;
  begin
    Result := DateTimeToStr (Dt1) = DateTimeToStr (Dt2);
  end;

  function MinutesDiff (aStart, aFinish :TTime) :integer;
  begin
    Result := Round (Abs (aStart - aFinish) * DayMinutes);
  end;

  constructor Date0__.Create;
  begin
    inherited Create;
    SetSeq (Year_, Month_, Day_);
  end;

  destructor Date0__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure Date0__.SetSeq (First, Second, Third :Ymd_);
  begin
    FSeq[Low (Seq_)] := First;
    FSeq[Succ (Low(Seq_))] := Second;
    FSeq[High(Seq_)] := Third;
  end;

  function Date0__.GetValue (aYmd :Ymd_) :YmdValue_;
  begin
    Result := FVal[aYmd];
  end;

  procedure Date0__.SetValue (aYmd :Ymd_; aValue :YmdValue_);
  begin
    FVal[aYmd] := aValue;
  end;

  function Date0__.GetSeqValue (aSeq :Seq_) :YmdValue_;
  begin
    Result := FVal[FSeq[aSeq]];
  end;

  procedure Date0__.SetSeqValue (aSeq :Seq_; aValue :YmdValue_);
  begin
    if (FSeq[aSeq] = Year_) then
      if  (aValue < Factor) then
        aValue := Factor + aValue;
    FVal[FSeq[aSeq]] := aValue;
  end;

  function Date0__.GetYear  :YmdValue_;
  begin
    Result := Value[Year_];
  end;

  procedure Date0__.SetYear (aValue :YmdValue_);
  begin
    Value[Year_] := aValue;
  end;

  function Date0__.GetMonth  :YmdValue_;
  begin
    Result := Value[Month_];
  end;

  procedure Date0__.SetMonth (aValue :YmdValue_);
  begin
    if (aValue <= 12) then
      Value[Month_] := aValue
    else begin
      Value[Month_] := aValue - 12;
      Value[Year_] := Value[Year_] + 1;
      end;
  end;

  function Date0__.GetDay  :YmdValue_;
  begin
    Result := Value[Day_];
  end;

  procedure Date0__.SetDay (aValue :YmdValue_);
  begin
    Value[Day_] := aValue;
  end;

  class function Date0__.FullYears (Months :YmdValue_) :YmdValue_;
  begin
    Result := (Abs (Months) - 1) DIV YearMonths;
  end;





  constructor DateTime1__.Create;
  begin
    inherited Create;
    Value := SysUtils.Now;
  end;

  function DateTime1__.DeDate (aDate :TDateTime; var aYear, aMon, aDay :YmdValue_) :boolean;
  var
    y, m, d :word;
  begin
    Result := false;
    try
      SysUtils.DecodeDate (aDate, y, m, d);
      aYear := y;
      aMon := m;
      aDay := d;
      Result := true;
    except
      on e :Exception do
        ;
      end;
  end;
                
  function DateTime1__.DeTime (aDate :TDateTime; var aHour, aMin, aSec, aMs :YmdValue_) :boolean;
  var
    Hr, Mi, Se, Ms :word;
  begin
    Result := false;
    try
      SysUtils.DecodeTime (aDate, Hr, Mi, Se, Ms);
      aHour := Hr;
      aMin := Mi;
      aSec := Se;
      aMs := Ms;
      Result := true;
    except
      on e :Exception do
        ;
      end;
  end;

  function DateTime1__.GetDate :TDateTime;
  begin
    Result := SysUtils.EncodeDate (FVal[Year_], FVal[Month_], FVal[Day_]) +
              SysUtils.EncodeTime (FHour, FMin, FSec, FMsec);
  end;

  procedure DateTime1__.SetDate (aDate :TDateTime);
  begin
    FValid := false;
    if DeDate (aDate, FVal[Year_], FVal[Month_], FVal[Day_]) then
      if DeTime (aDate, FHour, Fmin, FSec, FMSec) then
        FValid := true;
  end;

  procedure DateTime1__.SetTime (aDate :TTime);
  begin
    FValid := false;
    if DeTime (aDate, FHour, Fmin, FSec, FMSec) then
      FValid := true;
  end;

  function DateTime1__.GetDosDate :longint;
  begin
    Result := SysUtils.DateTimeToFileDate (Value);
  end;

  procedure DateTime1__.SetDosDate (aDate :longint);
  begin
    Value := SysUtils.FileDateToDateTime (aDate);
  end;

  procedure DateTime1__.SetYear (aYear :YmdValue_);
  var
    aMon, aDay :YmdValue_;
  begin
    if (aYear = 0) then begin {default is current year}
      if DeDate (SysUtils.Now, aYear, aMon, aDay) then
        ;
      end
    else if (aYear < Factor) then
      aYear := aYear + Factor;
    inherited SetYear (aYear);
  end;

  procedure DateTime1__.SetMonth (aMonth :YmdValue_);
  var
    Years   :YmdValue_;
  begin
    if (aMonth <= 0) then begin
      aMonth := aMonth - 1;
      Years := a0.Occupy (Abs (aMonth), YearMonths);
      aMonth := aMonth + Years * YearMonths;
      Year := Year - Years;
      end
    else if (aMonth > YearMonths) then begin
      Years := FullYears (aMonth);
      Year := Year + Years;
      aMonth := aMonth - Years * YearMonths;
      end;
    inherited SetMonth (aMonth);
    Day := Min (Day, Days);
  end;

  function DateTime1__.GetWeek :YmdValue_;
  var
    SaveDay    :YmdValue_;
    First      :YmdValue_;
  begin
    Result := 1;
    //start week uncertain
    SaveDay := Day;
    First := 1;
    inherited SetDay (First);
    while (WeekDay < Sun_) do begin
      Inc (First);
      inherited SetDay (First);
      end;
    if (SaveDay > First) then
      Result := Result + a0.Occupy (SaveDay - First, WeekDays);
    inherited SetDay (SaveDay);
  end;

  procedure DateTime1__.SetWeek (aValue :YmdValue_);
  var
    Wd      :YmdValue_;
    Months  :YmdValue_;
  begin
    //Value := Value + 7 * aValue;
    Wd := GetWeekDay;  //preserve day
    if (aValue <= 0) then begin
      aValue := aValue - 1;
      Months := a0.Occupy (Abs (aValue), MonthWeeks);
      aValue := aValue + Months * MonthWeeks;
      Month := Month - Months;
      end
    else if (aValue > MonthWeeks) then begin
      Months := a0.Occupy (aValue, MonthWeeks);
      aValue := aValue - Months * MonthWeeks;
      Month := Months;
      end;
    //1..4
    Day := Pred (aValue) * WeekDays + Wd;
  end;

  procedure DateTime1__.SetDay (aDay :YmdValue_);
  begin
    if (aDay <= 0) then begin
      Value := Value - (Day - aDay);
      end
    else if (aDay > Days) then
      Value := Value + (aDay - Day) //(Days - aDay)
    else
      inherited SetDay (aDay);
  end;

  function DateTime1__.GetQuarter :YmdValue_;
  begin
    Result := Succ (Pred (Month) DIV Pred (YearQuarters));
  end;

  procedure DateTime1__.SetQuarter (aValue :YmdValue_);
  var
    Years :YmdValue_;
  begin
    Day := 1;
    Years := (aValue - 1) DIV YearQuarters;
    if (Years > 0) then begin
      Year := Year + Years;
      aValue := aValue - Years * YearQuarters;
      end;
    Month := (aValue - 1) * Pred (YearQuarters) + 1;
  end;

  function DateTime1__.GetWeekDay :WeekDay_;
  begin
    Result := SysUtils.DayOfWeek (Value);  {1..7 = sun..Fri}
    Result := Pred (Result);
    if (Result = 0) then
      Result := Sun_;
  end;

  procedure DateTime1__.SetWeekDay (aWeekDay :WeekDay_);
  begin
    Day := Day + (aWeekDay - WeekDay);
  end;

  function ShortMonthToYmdValue (aMonth :string) :YmdValue_;
  var
    i  :1..YearMonths;
  begin
    Result := 0;
    for i := 1 to YearMonths do
      //if SameText (SysUtils.ShortMonthNames[i], aMonth) then begin
      if SameText (UTCMonths[i], aMonth) then begin
        Result := i;
        Exit;
        end;
    for i := 1 to YearMonths do
      if SameText (MyShortMonthNames[i], aMonth) then begin
        Result := i;
        Exit;
        end;
  end;

  function DateTime1__.Days :YmdValue_;
  var
    SaveAll   :YmdValues_;
    Start     :TDateTime;
  begin
    SaveAll := Raw;
    inherited SetDay (1);
    Start := Value;
    inherited SetMonth (Month + 1);             {inherited to avoid cycle}
    Result := Trunc (Value - Start);
    Raw := SaveAll;
  end;

  {function DateTime1__.GetYearWeek :YmdValue_;
  begin
    Result := Trunc (Value - SysUtils.EncodeDate (Year, 1, 1)) div WeekDays;
  end;}

  function DateTime1__.GetYearDay :YmdValue_;
  begin
    Result := DayOfTheYear (Value);
  end;

  function DateTime1__.GetCalendarWeek :SmallInt;
  {Code adapted from Ralph Friedman (TeamB) <ralphfriedman@email.com> }
  {Calculates calendar week assuming:
  - Monday is the 1st day of the week.
  - The 1st calendar week is the 1st week
    of the year that contains a Thursday.
    If result is 53, then previous year is assumed. }
  var
    First       :DateTime1__;
  begin
    First := DateTime1__.Create;
    First.Value := Value;
    case WeekDay of
      Sun_: First.Day := 2;
      Mon_: First.Day := 1;
      Tue_: First.Day := 31;
      Wed_: First.Day := 30;
      Thu_: First.Day := 29;
      Fri_: First.Day := 4;
      Sat_: First.Day := 3;
      end;
    if (First.Day > 4) then begin
      First.Year := First.Year - 1;
      First.Month := 12;
      end
    else
      First.Month := 1;
    if (First.Value > Value) then
      Result := 53
    else
      Result := (Trunc(Value - First.Value) div 7) + 1;
    First.Destroy;
  end;

const
  BadJulian = $FFFF;
  MinYear = 1600;
  First2Months = 59;         {1600 was a leap year}

  function DateTime1__.GetAsJulian :longint;
  begin
    if (Year < 100) then
      Inc (FVal[Year_], 1900);

    if (Year = MinYear) and (Month < 3) then
      if (Month = 1) then
        Result := Pred (Day)
      else
        Result := Day + 30
    else begin
      if Month > 2 then
        Dec (FVal[Month_], 3)
      else begin
        Inc (FVal[Month_], 9);
        Dec (FVal[Year_]);
        end;
      Dec (FVal[Year_], MinYear);
      Result := ((LongInt(Year div 100)*146097) div 4)+
        ((LongInt(Year mod 100)*1461) div 4)+
        (((153*Month)+2) div 5)+Day+First2Months;
      end;
  end;

  procedure DateTime1__.SetAsJulian (Julian :longint);
  var
    I, J : LongInt;
  begin
    if (Julian = 0) then
      Value := 0
    else if (Julian <= First2Months) then begin
      Year := MinYear;
      if Julian <= 30 then begin
        Month := 1;
        Day := Succ (Julian);
        end
      else begin
        Month := 2;
        Day := Julian - 30;
        end;
      end
    else begin
      I := (4*LongInt(Julian-First2Months))-1;
      J := (4*((I mod 146097) div 4))+3;
      FVal[Year_] := (100*(I div 146097))+(J div 1461);
      I := (5*(((J mod 1461)+4) div 4))-3;
      FVal[Month_] := I div 153;
      FVal[Day_] := ((I mod 153)+5) div 5;
      if Month < 10 then
        Inc (FVal[Month_], 3)
      else begin
        Dec (FVal[Month_], 9);
        Inc (FVal[Year_]);
        end;
      Inc (FVal[Year_], MinYear);
      end;
  end;





  constructor DateTime__.Create;
  begin
    inherited Create;
    d := SingleTokens__.CreateInc (DefaultDateSeps[sDate_]);
    t := SingleTokens__.CreateInc (DefaultDateSeps[sTime_]);
    s := Tokens__.CreateInc (DefaultDateSeps[sDateTime_]);
    m := SingleTokens__.CreateInc (DefaultDateSeps[sMilli_]);
  end;

  destructor DateTime__.Destroy;
  begin
    m.Free;
    s.Free;
    t.Free;
    d.Free;
    inherited Destroy;
  end;

  procedure DateTime__.SetDateSep (aValue :string);
  begin
    d.Separator := aValue;
  end;

  procedure DateTime__.SetTimeSep (aValue :string);
  begin
    t.Separator := aValue;
  end;

  procedure DateTime__.SetDateTimeSep (aValue :string);
  begin
    s.Separator := aValue;
  end;

  function DateTime__.GetTimeStr :String;
  begin
    Result := NumStr__.AsString (Hour, 2);
    if (t.Separator <> '') then
      Result := Result + t.Separator;
    Result := Result + NumStr__.AsString  (Minutes, 2);
    if (t.Separator <> '') then
      Result := Result + t.Separator;
    Result := Result +  NumStr__.AsString (Seconds, 2);
    if FHasMil then begin
      if (m.Separator <> '') then
        Result := Result + m.Separator;
      Result := Result + NumStr__.AsString (FMSec, 1);
      end;
  end;

  function DateTime__.GetNextNum (var aNum :YmdValue_; var aSource :string) :boolean;
  begin
    Result := false;
    if not Str__.IsBlank (aSource) then begin
      aNum := SysUtils.StrToIntDef (System.Copy (aSource, 1, 2), 1);
      System.Delete (aSource, 1, 2);
      Result := true;
      end;
  end;

  procedure DateTime__.SetTimeStr (aTime :string);
  var
    v :Ymdvalue_;
  begin
    FHour := 0;
    FMin := 0;
    FSec := 0;
    FMsec := 0;
    FHasMil := false;
    if (t.Separator = '') then begin
      if GetNextNum (v, aTime) then begin
        Hour := v;
        if GetNextNum (v, aTime) then begin
          Minutes := v;
          if GetNextNum (v, aTime) then
            Seconds := v;
          end;
        end;
      end
    else begin
      t.Line := aTime;
      if t.Next then begin
        Hour := t.Number;
        if t.Next then begin
          Minutes := t.Number;
          if t.Next then begin
            m.Line := t.Token;
            Seconds := m.NextNumber;
            if m.Next then begin
              FMsec := m.NUMBER;
              FHasMil := true;
              end;
            end;
          end;
        end;
      end;
  end;

  function DateTime__.GetDateStr :String;
  var
    i :Seq_;
  begin
    Result := '';
    for i := Low (Seq_) to High (Seq_) do begin
      if (FSeq[i] = Month_) and FStrMon then begin
        Str__.Append (Result, Str__.Ch (d.Separator, 1), {SysUtils.}MyShortMonthNames[GetSeqValue(i)]);
        end
      else
        Str__.Append (Result, Str__.Ch (d.Separator, 1), NumStr__.AsString  (GetSeqValue (i), 2));
      end;
  end;

  function DateTime__.ImplicitYear :YmdValue_;
  begin
    Result := 0;     {this year}
  end;

  procedure DateTime__.SetDateStr (aDate :String);
  var
    i       :ShortInt;
    aMonth  :string;
    Number  :YmdValue_;
  begin
    if (d.Separator = '') then begin
      i := Low (Seq_);
      while (i <= High (Seq_)) and GetNextNum (Number, aDate) do begin
        SetSeqValue (i, Number);
        Inc (i);
        end;
      end
    else begin
      d.Line := aDate;
      i := Low (Seq_);
      while (i <= High (Seq_)) and d.Next do begin
        if (FSeq[i] = Month_) then begin
          aMonth := d.TOKEN;
          if A0.DIG (Str__.Ch (aMonth, 1)) then begin
            SetSeqValue (i, d.NUMBER);
            FStrMon := false;
            end
          else begin
            SetSeqValue (i, ShortMonthToYmdValue (aMonth));
            FStrMon := true; {detected}
            end;
          end
        else
          SetSeqValue (i, d.NUMBER);
        Inc (i);
        end;
      end;
  end;

  function DateTime__.GetDateTimeStr :String;
  begin
    Result := DateStr;
    if (s.Separator <> '') then
      Result := Result + s.Separator;
    Result := Result + TimeStr;
  end;

  procedure DateTime__.SetDateTimeStr (aDateTime :string);
  begin
    s.Line := aDateTime;
    if s.Next then begin
      DateStr := s.TOKEN;
      {date time sep may be coincide with other}
      TimeStr := System.Copy (aDateTime, s.LEN + 2, Length (aDateTime));
      end;
    {if s.NXT then
      TimeStr := s.TOKEN;}
  end;

  function DateTime__.Parse (aValue :string) :boolean;
  var
    i  :ShortInt;
  begin
    Result := false;
    for i := 1 to YearMonths do
      if Str__.IsIn (StdSettings.ShortMonthNames[i], aValue, true) then begin
        Month := i;     {will set latest month}
        Result := true;
        end;
  end;

  function DateTime__.Rfc822 :string;
  const
    MyShortDayNames: array[1..WeekDays] of string =
      ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
  var
    i                   :ShortInt;
    SaveShortDayNames   :array[WeekDay_] of string;
    SaveShortMonthNames :array[1..YearMonths] of string;
    Replace             :boolean;
  begin
    Replace := (StdSettings.ShortDayNames[1] <> MyShortDayNames[1]);
    if Replace then begin
      for i := Low(StdSettings.ShortDayNames) to High(StdSettings.ShortDayNames) do begin
        SaveShortDayNames[i] := StdSettings.ShortDayNames[i];
        StdSettings.ShortDayNames[i] := MyShortDayNames[i];
        end;
      for i := Low(StdSettings.ShortMonthNames) to High(StdSettings.ShortMonthNames) do begin
        SaveShortMonthNames[i] := StdSettings.ShortMonthNames[i];
        StdSettings.ShortMonthNames[i] := MyShortMonthNames[i];
        end;
      end;
    Result := SysUtils.FormatDateTime ('ddd, d mmm yyyy hh:nn:ss', Value);
    if Replace then begin
      for i := Low(StdSettings.ShortDayNames) to High(StdSettings.ShortDayNames) do
        StdSettings.ShortDayNames[i] := SaveShortDayNames[i];
      for i := Low(StdSettings.ShortMonthNames) to High(StdSettings.ShortMonthNames) do
        StdSettings.ShortMonthNames[i] := SaveShortMonthNames[i];
      end;
  end;


  function DateTime__.GetWeekDayStr :string;
  begin
    Result := StdSettings.ShortDayNames[AsIndex (WeekDay)];
  end;

  procedure DateTime__.SetWeekDayStr (aValue :string);
  var
    d   :WeekDay_;
  begin
    for d := Mon_ to Sun_ do
      if CompareText (StdSettings.ShortDayNames[AsIndex (d)], aValue) = 0 then begin
        WeekDay := d;
        Exit;
        end;
  end;







(*

Some functions will return a TFileTime type (_FILETIME structure in WinAPI) such as GetProcessTimes(),
GetThreadTimes(), GetFileTime(), etc. This function will convert that value to a readable date/time string. Read the
comment for the usage. You can change the format of the resulting date string in the FormatDateTime() function any way you
please. Thanks to Phil Stubbington <http://www.ambitus.co.uk> for this code. If you want just the modified date of a file, see
GetModifiedDate() on page 2.

function FileTimeToLongStr(ft : TFileTime): string;
{ func to convert TFileTime to readable date/time. }
{ Thanks to Phil Stubbington for this code. }
{ Email: delphi@ambitus.com Web: http://www.ambitus.co.uk }
{ Usage:
  procedure TForm1.Button1Click(Sender: TObject);
  var
    fs: TFileStream;
    CreateTime,
    LastAccessTime,
    WriteTime: TFileTime;
    CreateTimeStr,
    LastAccessTimeStr,
    WriteTimeStr: string;
  begin
    fs := TFileStream.Create('C:\SomeDir\SomeFile.tmp', fmOpenRead or fmShareDenyNone);
    try
      GetFileTime(fs.Handle, @CreateTime, @LastAccessTime, @WriteTime);
      CreateTimeStr := FileTimeToLongStr(CreateTime);
      LastAccessTimeStr := FileTimeToLongStr(LastAccessTime);
      WriteTimeStr := FileTimeToLongStr(WriteTime);
      { ... }
    finally
      fs.Destroy;
    end;
  end; }
var
  st: TSystemTime;
begin
  { convert FileTime to local time zone }
  if FileTimeToLocalFileTime(ft, ft) then
    { convert to SystemTime }
    if FileTimeToSystemTime(ft, st) then
      { Convert to TDateTime }
      Result := FormatDateTime('dd mmmm yyyy hh:mm:ss', SystemTimeToDateTime(st))
    else
      Result := ''
  else
    Result := '';
end;

*)
initialization
finalization
end.





