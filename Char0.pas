unit Char0;
interface
type
  Chars_ = set of Char;
  CharSet_ = packed set of Char;
  Quotes_ = packed record
    case boolean of
     true:  (Open :Char; Close :Char);
     false: (Str :packed array [1..2] of Char);
    end;
  Response_ = (Esc_, Yes_, No_);
const
  NullCh = #0;
  Bs = #8;
  Cr = #13;
  Esc = #27;
  Ff = #12;
  Xon = ^Q;
  Xoff = ^S;
  Lf = #10;
  Tab = #9;    {Tab = ^I;}
  CrLf = Cr + Lf;
  Eol = Cr + Lf;
  Space = #32;
  SpaceTab = Space + Tab;

  DoubleQuote = '"';
  SingleQuote = '''';

  Ampersand = '&';
  AngleOpen = '<';
  AngleClose = '>';
  Apostroph = SingleQuote;
  AtSign = '@';
  BackSlash = '\';
  BracketOpen = '[';
  BracketClose = ']';
  BraceOpen = '{';
  BraceClose = '}';
  Colon = ':';
  Comma = ',';
  Dollar = '$';
  Dot = '.';
  Equal = '=';
  Exclamation = '!';
  Minus = '-';
  PercentChar =  '%';
  ParenOpen = '(';
  ParenClose = ')';
  Plus = '+';
  PipeChar = '|';
  Question = '?';
  SemiColon = ';';
  Slash = '/';
  Sharp = '#';
  Asterisk = '*';
  Tilde = '~';
  Underscore = '_';

  Any = Asterisk;
  One = PercentChar;
const
  SpecialChars = [PercentChar, Ampersand, Colon, SingleQuote, ParenOpen, ParenClose,
    BraceOpen, BraceClose, BracketOpen, BracketClose, DoubleQuote, AngleOpen, AngleClose, Tab, Cr, Lf];
  CapitalLetters = ['A'..'Z'];
  SmallLetters  = ['a'..'z'];
  Letters = CapitalLetters + SmallLetters;
  EuropeanLetters = ['�'..'�'];
  DigitsSet = ['0'..'9'];
  SignSet = [Minus, Plus];
  DigitSeparators = [Dot, Comma];
  HexDigits = DigitsSet + ['A'..'F', 'a'..'f'];
  HexDigitsStr :string = '0123456789ABCDEF';
  Alphanumerics = Letters + DigitsSet;
  IdentifierFirst = Letters + [Underscore];
  Identifiers = Alphanumerics + [Underscore];
  UnitIdentifiers = Identifiers + [Dot];
  Syllables = ['a', 'e', 'i', 'o', 'u', 'y'];
  VisibleSet = [#32..#127];

  NumericSet = SignSet + DigitsSet + DigitSeparators;
  FloatCharSet = SignSet + DigitsSet + [Dot, 'e','E'];   //no Comma
const
  Assignment = ':=';
  AngleBrackets :Quotes_ = (Open :AngleOpen; Close :AngleClose);
  SquareBrackets :Quotes_ = (Open: BracketOpen; Close: BracketClose);
  Parentheses :Quotes_ = (Open: ParenOpen; Close :ParenClose);
  Braces :Quotes_ = (Open: BraceOpen; Close :BraceClose);
  SingleQuotes :Quotes_ = (Open: SingleQuote; Close :SingleQuote);
  DoubleQuotes :Quotes_ = (Open: DoubleQuote; Close :DoubleQuote);
type
  Char__ = class (TObject)
   private
   protected
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    class function LoCase (aValue :char) :char;
    class function Value (aValue :char) :byte;
    class function HighAscii (aValue :char) :boolean;
    class function AsLowAscii (aValue :char) :char;
    end;


implementation
uses SysUtils;

  constructor Char__.Create;
  begin
    inherited Create;
  end;

  destructor Char__.Destroy;
  begin
    inherited Destroy;
  end;

  class function Char__.LoCase (aValue :char) :char;
  const
    Shift = Ord('a') - Ord('A');
  begin
    Result := aValue;
    if (aValue in CapitalLetters) then
      Result := Chr (Ord (aValue) + Shift);
  end;

  class function Char__.Value (aValue :char) :byte;
  const
    Shift = Ord('0');
  begin
    Result := 0;
    if (aValue in DigitsSet) then
      Result := (Ord (aValue) - Shift);
  end;

  class function Char__.HighAscii (aValue :char) :boolean;
  begin
    Result := false;
    if (Ord (aValue) > 127) then
      Result := true;
  end;

  class function Char__.AsLowAscii (aValue :char) :char;
  type
    Range_ = record
      l :char;
      h :char;
      c :char;
      end;
  const
    Ranges :array [1..19] of Range_ = (
      (l: #$C0; h : #$C5; c: 'A'),
      (l: #$C7; h : #$C7; c: 'C'),
      (l: #$C8; h : #$CB; c: 'E'),
      (l: #$CC; h : #$CF; c: 'I'),
      (l: #$D0; h : #$D0; c: 'D'),
      (l: #$D1; h : #$D1; c: 'N'),
      (l: #$D2; h : #$D6; c: 'O'),
      (l: #$D8; h : #$D8; c: 'O'),
      (l: #$D9; h : #$DC; c: 'U'),
      (l: #$DD; h : #$DD; c: 'Y'),
      (l: #$E0; h : #$E5; c: 'a'),
      (l: #$E7; h : #$E7; c: 'c'),
      (l: #$E8; h : #$EB; c: 'e'),
      (l: #$EC; h : #$EF; c: 'i'),
      (l: #$F1; h : #$F1; c: 'n'),
      (l: #$F2; h : #$F6; c: 'o'),
      (l: #$F9; h : #$FC; c: 'u'),
      (l: #$FD; h : #$FD; c: 'y'),
      (l: #$FF; h : #$FF; c: 'y')
      );
  var
    i     :ShortInt;
    Found :boolean;
  begin
    Result := aValue;
    if HighAscii (Result) then begin
      Found := false;
      i := 1;
      while not Found and (i <= 19) do
        if (Result >= Ranges[i].l) and (Result <= Ranges[i].h) then begin
          Result := Ranges[i].c;
          Found := true;
          end
        else
          Inc (i);
      end;
  end;

initialization
finalization
end.












