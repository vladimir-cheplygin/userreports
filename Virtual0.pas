unit Virtual0;
interface
{..$M+}
type
  Virtual__ = class (TObject)
   private
   protected
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    function GetClone :Virtual__;
    end;

  VirtualClass_ = class of Virtual__;

implementation
{uses
  Windows;}

  constructor Virtual__.Create;
  {var
    ClName :string;}
  begin
    inherited Create;
    {ClName := ClassName;
    OutputDebugString(PChar(Clname));}
  end;

  destructor Virtual__.Destroy;
  begin
    inherited Destroy;
  end;

  function Virtual__.GetClone :Virtual__;
  var
    TheClass :VirtualClass_;
  begin
    TheClass := VirtualClass_(ClassType);
    Result := TheClass.Create;
    //Result. Assign (Self);
  end;



initialization
finalization
end.
