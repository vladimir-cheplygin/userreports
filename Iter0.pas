unit Iter0;
interface
uses
  Event0,
  Virtual0;
const
  Undefined_ = -1;
type
  Iterator__ = class;

  Container__ = class (Virtual__)
   private
   protected
    function Accept (aIter :Iterator__) :boolean;
      virtual;
    function GetNewIterator :Iterator__;
      virtual;
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    property NewIterator :Iterator__ read GetNewIterator;
    end;

  Iterator__ = class (TObject)
   protected
    FContainer    :TObject;
    _Valid        :boolean;
    function GetContainer :Container__;
    procedure SetContainer (aValue :Container__);
      virtual;
   private
    _Started      :boolean;
    _Finished     :boolean;
    _Rewind   :record
      Auto      :boolean;
      Immediate :boolean;
      Active    :boolean;
      end;
    _OnRewind     :FeedbackEvent_;
    procedure SetRewindAuto (aValue :boolean);
    procedure SetRewindLazy (aValue :boolean);
   private
    _OnFilter     :FeedbackEvent_;
    _OnNext       :FeedbackEvent_;
    _OnFinish     :FeedbackEvent_;
    function NextFiltered :boolean;
    procedure SetValid (aValue :boolean);
   protected
    procedure SetOnFinish (aValue :FeedbackEvent_);
      virtual;
    function FilteredCustom :boolean;
      virtual;
    function NextOk :boolean;
    function GetValid :boolean;
      virtual;
    function GetIndex :integer;
      virtual;
    function GetCurrent :TObject;
      virtual;
    procedure RewindCustom;
      virtual;
    function NextCustom :boolean;
      virtual;
   public
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    procedure Rewind;
    function Next :boolean;
    property Index :integer read GetIndex;
    property Current :TObject read GetCurrent;
    property OnRewind :FeedbackEvent_ write _OnRewind;
    property OnFilter :FeedbackEvent_ read _OnFilter write _OnFilter;
    property OnFinish :FeedbackEvent_ read _OnFinish write SetOnFinish;
    property OnNext :FeedbackEvent_ read _OnNext write _OnNext;
    property AutoRewind :boolean read _Rewind.Auto write SetRewindAuto;
    property LazyRewind :boolean write SetRewindLazy;
    property Container :Container__ read GetContainer write SetContainer;
    property Valid :boolean read GetValid write SetValid;
    function Enumerate :cardinal;
      virtual;
    property Started :boolean read _Started;
    property Finished :boolean read _Finished;
    procedure SetFinished;
      virtual;
    procedure SetUnFinished;
    end;

implementation

  constructor Container__.Create;
  begin
    inherited Create;       //resource leak
  end;

  destructor Container__.Destroy;
  begin
    inherited Destroy;
  end;

  function Container__.Accept (aIter :Iterator__) :boolean;
  begin
    Result := true;
  end;

  function Container__.GetNewIterator :Iterator__;
  begin
    Result := nil;
  end;



  constructor Iterator__.Create;
  begin
    inherited Create;                          //resource leak
  end;

  destructor Iterator__.Destroy;
  begin
    inherited Destroy;
  end;

  procedure Iterator__.SetRewindAuto (aValue :boolean);
  begin
    _Rewind.Auto := aValue;
  end;

  procedure Iterator__.SetRewindLazy (aValue :boolean);
  begin
    _Rewind.Immediate := not aValue;
  end;

  procedure Iterator__.RewindCustom;
  begin
  end;

  procedure Iterator__.Rewind;
  begin
    if not _Rewind.Active then begin
      _Rewind.Active := true;
      _Started := false;
      _Finished := false;
      if _Rewind.Immediate then
        RewindCustom;
      _Rewind.Active := false;
      end;
  end;

  function Iterator__.GetContainer :Container__;
  begin
    Result := FContainer as Container__;
  end;

  procedure Iterator__.SetContainer (aValue :Container__);
  begin
    FContainer := aValue;
    _Valid := (FContainer <> nil);
    Rewind;
  end;

  function Iterator__.GetIndex :integer;
  begin
    Result := Undefined_;
  end;

  function Iterator__.GetCurrent :TObject;
  begin
    Result := nil;
  end;

  procedure Iterator__.SetValid (aValue :boolean);
  begin
    _Valid := aValue;
  end;

  function Iterator__.GetValid :boolean;
  begin
    Result := false;
    if _Valid then
      Result := true
    else if (FContainer <> nil) then
      Result := true;
  end;

  procedure Iterator__.SetFinished;
  begin
    if _Finished then
      Skip
    else begin
      _Finished := true;
      if Assigned (_OnFinish) then
        if not _OnFinish (Self) then
          ; //_Finished := false;
      end;
  end;

  procedure Iterator__.SetUnFinished;
  begin
    if _Finished then
      _Finished := false
    else
      Skip;
  end;

  procedure Iterator__.SetOnFinish (aValue :FeedbackEvent_);
  begin
    _OnFinish := aValue;
  end;

  function Iterator__.NextCustom :boolean;
  begin
    Result := false;
  end;

  function Iterator__.FilteredCustom :boolean;
  begin
    Result := true;
    if Assigned (_OnFilter) then
      if not _OnFilter (Self) then
        Result := false;
  end;

  function Iterator__.NextOk :boolean;
  begin
    Result := true;
    if Assigned (_OnNext) then
      if not _OnNext (Self) then
        Result := false;
  end;

  function Iterator__.NextFiltered :boolean;
  var
    Done :boolean;
  begin
    Result := false;
    Done := false;
    while not Result and not Done do
      if not NextCustom then
        Done := true
      else if FilteredCustom then
        Result := true;
  end;

  function Iterator__.Next :boolean;
  begin
    Result := false;
    if Valid then
    if (not _Finished) then begin
      if not _Started then begin
        _Started := true;
        if not _Rewind.Immediate then
          RewindCustom;
        if Assigned (_OnRewind) then
          if not _OnRewind (Self) then
           ;
        end;
      if NextFiltered then                        //internal
        if NextOk then
          Result := true;
      if not Result then begin
        SetFinished;
        if AutoRewind then
          Rewind;
        end;
      end;
  end;
  (*
  procedure Iterator__.Iterate (Sender :TObject);
  begin
    Rewind;
    while Next do
     ;
  end;
  *)
  function Iterator__.Enumerate :cardinal;
  begin
    Result := 0;
    Rewind;
    while Next do
      Inc (Result);
    Rewind;
  end;


end.


