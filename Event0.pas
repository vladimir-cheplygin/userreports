unit Event0;
{$ifdef FPC} {$MODE Delphi} {$endif}
interface
uses
  Classes,
  ExtCtrls;
const
  MyTest = false;
type
  ObjectDesc_ = function (o :TObject) :string;
  FeedbackEvent_ = function (Sender :TObject) :boolean of object;
  FeedbackEvent__ = class (TObject)
   private
    _Performing :boolean;
   protected
   public
    Value    :FeedbackEvent_;
    Client   :TObject;
    constructor Create;
      virtual;
    destructor Destroy;
      override;
    property Performing :boolean read _Performing;
    function Perform  (Arg :TObject) :boolean;
    end;

  Delayed__ = class (FeedbackEvent__)
   private
    _Timer :TTimer;
    procedure OnTimerEvent (Sender :TObject);
    procedure SetDelay (aValue :integer);
    function GetActive :boolean;
    procedure SetActive (aValue :boolean);
   private
    _OnChange :FeedbackEvent_;
   protected
   public
    constructor Create;
      override;
    destructor Destroy;
      override;
    property Delay :integer write SetDelay;
    property Active :boolean read GetActive write SetActive;
    property OnChange :FeedbackEvent_ write _OnChange;
    end;



  //function Debug (Cont :boolean = true) :boolean;
  procedure Debug (const aMsg :string = '');
  procedure Debug2 (const aMsg :string = '');
  function DebugOk (aCond :boolean = true) :boolean;
  function DebugEvent (aCond :boolean) :boolean;
  procedure Skip;
  procedure Warn;
  procedure Slowdown (aValue :integer);
  function GetInfo (aValue :TObject) :string;


const
  __MyPlane = 2;
  __ObjMax = 1;
type
  ObjRec_ = record
    Obj :TObject;
    Act :boolean;
  end;
var
  __IsIteration :boolean;
  __Itera    :cardinal;
  __TestCase :cardinal;
  __Plane    :word;
  __New      :boolean;
  __Brain    :cardinal;
  __Layer    :cardinal;
  __DistalCnt:integer;

  __Obj    :array [0..__ObjMax] of ObjRec_;
  __ObjQ   :cardinal;
  Act0, Act1, Act2, Act3 :array[0..1, 0..1] of string;

  function IsSetMy :boolean;
  function SetMy (aValue :TObject; i :cardinal) :boolean;
  procedure DelMy (aValue :TObject);
  function GetMy (aValue :TObject; out j :cardinal) :boolean;
  function IsMy (aValue :TObject) :boolean;
  function MyActivate(aValue :TObject; aActive :boolean) :boolean;
  procedure FreeMy;
  function MyPlane :boolean;
  function MyActive :boolean;

type
  ObjectCheck_ = function (aObj :TObject) :boolean;
var
  ObjectCheck :ObjectCheck_;


implementation
uses
  Windows,
  SysUtils;
  ///JclDebug,
  ///Log0;


  function MyPlane :boolean;
  begin
    if __Plane <> __MyPlane then
      Result := false
    else if  __New then
      Result := false
    else
      Result := true;
  end;

  procedure Init(var ObjRec :ObjRec_);
  begin
    ObjRec.Obj := nil;
    ObjRec.Act := false;
  end;

  procedure FreeMy;
  var
    i :cardinal;
  begin
    for i := 0 to __ObjMax do
      Init(__Obj[i]);
    __ObjQ := 0;
  end;

  function SetMy (aValue :TObject; i :cardinal) :boolean;
  begin
    {if not MyPlane then
      Result := false
    else} if (i <> __ObjQ) then
      Result := false
    else if (__Obj[i].Obj <> nil) then
      Result := false                 //already set
    else if (aValue = nil) then
      Result := false                //only set non nil
    else begin
      __Obj[i].Obj := aValue;
      __ObjQ := Succ(i);
      Result := true;
    end;
  end;

  function IsSetMy :boolean;
  var
    i :cardinal;
  begin
    Result := true;
    for i := 0 to __ObjMax do
      if (__Obj[i].Obj = nil) then begin
        Result := false;
        Break;
      end;
  end;

  procedure DelMy (aValue :TObject);
  var
     i :cardinal;
     j :cardinal;
  begin
    for i := __ObjMax downto 0 do
      if (aValue = __Obj[i].Obj) then begin
        for j := i to __ObjMax do
          Init(__Obj[j]);
        __ObjQ := i;
        Break;
        end;
  end;

  function GetMy (aValue :TObject; out j :cardinal) :boolean;  //in the stack
  var
    i :cardinal;
  begin
    if (aValue = nil) then
      Result := false
    else if (__ObjQ = 0) then
      Result := false
    else begin
      Result := false;
      for i := Pred(__ObjQ) downto 0 do
        if (aValue = __Obj[i].Obj) then begin
          j := i;
          Result := true;
          Break;
          end;
      end;
  end;

  function IsMy (aValue :TObject) :boolean;
  var
    j :cardinal;
  begin
    if GetMy (aValue, j) then
      Result := true
    else
      Result := false
  end;

  function MyActivate(aValue :TObject; aActive :boolean) :boolean;
  var
    i   :cardinal;
    j   :cardinal;
    PrevOk :boolean;
  begin
    if not GetMy (aValue, j) then
      Result := false
    else if aActive then begin   //prev must be active
      if (j = 0) then
        PrevOk := true
      else begin
        PrevOk := true;
        for i := 0 to Pred(j) do
          if not __Obj[i].Act then
            PrevOk := false;
        end;
      if not PrevOk then
        Result := false
      else begin
        __Obj[j].Act := aActive;
        Result := true;
        end;
      end
    else begin                         //prev can be any
      if not __Obj[j].Act then         //has not been activated?
        Result := false
      else begin
        __Obj[j].Act := aActive;
        Result := true;
        end;
      end;
  end;

  function MyActive :boolean;
  var
    i  :cardinal;
  begin
    if (__ObjQ < 2) then
      Result := false
    else begin
      Result := false;
      i := Pred (__ObjQ);
      if __Obj[i].Act then
        Result := true;
    end;
  end;




  constructor FeedbackEvent__.Create;
  begin
    inherited Create;
  end;

  destructor FeedbackEvent__.Destroy;
  begin
    inherited Destroy;
  end;

  function FeedbackEvent__.Perform (Arg:TObject) :boolean;
  begin
    Result := false;
    if not _Performing then begin
      _Performing := true;
      Result := true;
      if Assigned (Value) then
        if not Value (Arg) then
          Result := false;
      _Performing := false;
      end;
  end;




  constructor Delayed__.Create;
  begin
    inherited Create;
    _Timer := TTimer.Create (nil);
    _Timer.Enabled := false;
    _Timer.OnTimer := OnTimerEvent;
  end;

  destructor Delayed__.Destroy;
  begin
    Active := false;
    inherited Destroy;
    _Timer.Destroy;
  end;

  procedure Delayed__.SetDelay (aValue :integer);
  begin
    _Timer.Interval := aValue;
  end;

  function Delayed__.GetActive :boolean;
  begin
    Result := _Timer.Enabled;
  end;

  procedure Delayed__.SetActive (aValue :boolean);
  begin
    _Timer.Enabled := false;
    _Timer.Enabled := aValue;
  end;

  procedure Delayed__.OnTimerEvent (Sender :TObject);
  begin
    Active := false;
    if Assigned (_OnChange) then begin
      if _OnChange (Self) then
        ;
      end;
  end;





  {function Debug (Cont :boolean = true) :boolean;
  begin
    Result := true;
    if not Cont then
      Result := false;
  end;}

  function GetCurrentStack: string;
  {
  var
     stackList: TJclStackInfoList; //JclDebug.pas
     sl: TStringList;
     }
  begin
     {
     stackList := JclCreateStackList(False, 0, Caller(0, False));
     sl := TStringList.Create;
     stackList.AddToStrings(sl, True, True, True, True);
     Result := sl.Text;
     sl.Free;
     stacklist.Free;
     }
    Result := '';
  end;

  procedure Debug (const aMsg :string = '');
  begin
    if (aMsg <> '') then
      OutputDebugString (PChar(aMsg))
    else
     {$ifdef Console}
      WriteLn(GetCurrentStack);
     {$else}
      OutputDebugString (PChar(GetCurrentStack))
     {$endif}
  end;

  procedure Debug2 (const aMsg :string = '');
  begin
   if IsConsole then begin
     WriteLn (aMsg);
     WriteLn (GetCurrentStack);
     end;
  end;

  function DebugOk (aCond :boolean = true) :boolean;
  begin
    Result := aCond;
    if not aCond then
      Debug;
  end;

  function DebugEvent (aCond :boolean) :boolean;
  begin
    Result := false;
    if aCond then
      if (GetKeyState (VK_CONTROL) < 0) then
        Result := true;
  end;

  procedure Skip;
  begin
    //Debug;
  end;

  procedure Warn;
  begin
    //Debug;
  end;

  function GetInfo (aValue :TObject) :string;
  begin
    if (aValue = nil) then
      Result := 'nil'
    else begin
      Result := aValue.ClassName;
      if (aValue is TComponent) then
        Result := Result + '.' + TComponent (aValue).Name;
      end;
  end;

  procedure Slowdown (aValue :integer);
  begin
    Sleep (1 * aValue);
  end;

initialization
  FreeMy;
finalization
end.










