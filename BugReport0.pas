unit BugReport0;
interface
uses
  Classes,
  String0,
  Tim0,
  Tk00,
  Version0;
const
  Txt = '.txt';
  Png = '.png';
type
  PC__ = class (TObject)
  private
    _Name      :string;
    _Chip      :string;
    _Memory    :string;
    _OS        :string;
    function get_ID: string;
  public
    property Name   :string read _Name write _Name;
    property Memory :string read _Memory write _Memory;
    property Chip   :string read _Chip write _Chip;
    property OS     :string read _OS write _OS;
    property ID: string read get_ID;
  end;

  VersionedApp__ = class (TObject)
  private
    _Name    :string;
    _Date    :DateTime__;
    _Version :Version__;
    _Error   :DateTime__;
    procedure set_Name (aValue :string);
  protected
  public
    constructor Create;
    destructor Destroy; override;
    property Name :string read _Name write set_Name;
    property Date :DateTime__ read _Date;
    property Error :DateTime__ read _Error;
    property Version :Version__ read _Version;
    function MatchesVersion (aValue :string) :boolean;
  end;

  StrProp_ = (Executable_, Version_, ExcClass_, ExcMessage_, ExcOwner_, Unique_, PCName_, PCMemory_, PCChip_, PCOS_);

  BugReport__ = class (TObject)
  private
    _IsValid :boolean;
    _Exe     :VersionedApp__;
    _PC      :PC__;
    function get_Dir: string;
    function get_ID :string;
  private
    _ErrorDateSet  :boolean;
    function get_ErrorDateAsString :string;
    procedure set_ErrorDateAsString (aValue: string);
    function GetDateFormat (const Orig :string) :string;
  private
    _Title      :string;
    _ExcClass   :string;
    _ExcMessage :string;
    _ExcNumber: Integer;
    _Owner      :string;
    procedure set_Title(aValue: string);
    procedure set_ExcClass (const aValue :string);
    function get_Title :string;
    function get_Version :string;
    function get_Owner :string;
    function get_Executable :string;
    function IsValidProcName (const aProc :string) :boolean;
    function IsValidOffset (const aOffset :string) :boolean;
  private
    _UserName   :string;
    _UserMail   :string;
    function get_User: string;
    procedure set_UserMail (aValue: string);
  private
    _Lines    :Str__;
    _Index    :integer;
    _Tokens   :Tokens__;
    _Stack    :Str__;
    _Crc      :string;
    _Depth    :cardinal;
    _Root     :string;
    function Next (out Line: string) :boolean;
    function GetProcName (const Line :string; out Proc :string) :boolean;
    function get_Unique :string;
  protected
    function ParseData :boolean;
    function get_Prop (AProp  :StrProp_) :string;
  public
    constructor Create;  virtual;
    destructor Destroy;
      override;
    function LoadFromStream (const aValue :TStream) :Boolean;
    function LoadFromFile(const AName :string) :boolean;
    function Before (Day :DateTime__) :boolean;
    function After (Day :DateTime__) :boolean;
    property IsValid :boolean read _IsValid;
    property Exe :VersionedApp__ read _Exe;
    property PC :PC__ read _PC;

    property Executable :string read get_Executable;
    property Version :string read get_Version;
    property ErrorDateAsString :string read get_ErrorDateAsString write set_ErrorDateAsString;
    property Title :string read get_Title write set_Title;
    property ExcClass :string read _ExcClass write set_ExcClass;
    property ExcMessage: string read _ExcMessage;
    property ExcNumber: Integer read _ExcNumber;
    property Owner: string read get_Owner;
    property User: string read get_User;
    property Unique: string read get_Unique;
    property Stack :Str__ read _Stack;
    property Depth: Cardinal write _Depth;

    property ID :string read get_ID;
    property Dir: string read get_Dir;

    property Prop[i: StrProp_] :string read get_Prop;
  end;


implementation
uses
  SysUtils,
  A0,
  Char0;
  //Fx00;

var
  __Parens :PairTokens__;

  function PC__.get_ID: string;
  begin
    Result := _Name + _Chip + _Memory;
  end;



  constructor VersionedApp__.Create;
  begin
    inherited Create;
    _Version := Version__.Create;
    _Date := DateTime__.Create;
    _Error := DateTime__.Create;
  end;

  destructor VersionedApp__.Destroy;
  begin
    _Date.Free;
    _Error.Free;
    _Version.Free;
    inherited Destroy;
  end;

  procedure VersionedApp__.set_Name(aValue :string);
  begin
    {if (__Spec = nil) then
      __Spec := FileSpec__.Create;
    __Spec.FileName := aValue;
    _Name := __Spec.Nam.Value}
    _Name := ChangeFileExt(aValue, '');
  end;

  function VersionedApp__.MatchesVersion (aValue :string) :boolean;
  begin
    if Str__.Match (aValue, _Version.AsString, true) then
      Result := true
    else
      Result := false
  end;




  constructor BugReport__.Create;
  begin
    inherited Create;
    _Exe := VersionedApp__.Create;
    _PC := PC__.Create;
    _Tokens := Tokens__.Create;
    _Tokens.Backwards := true;
    _Lines := Str__.Create;
    _Stack := Str__.Create;
    _Stack.Delimiter := ' ';
    _Depth := 7;
    _Root := 'initialization';
  end;

  destructor BugReport__.Destroy;
  begin
    _Tokens.Free;
    _Exe.Free;
    _PC.Free;
    _Lines.Free;
    _Stack.Free;
    inherited Destroy;
  end;

  procedure BugReport__.set_ErrorDateAsString (aValue: string);
  begin
    if Str__.IsBlank (aValue) then
      _ErrorDateSet := false
    else begin
      _ErrorDateSet := true;
      _Exe.Error.AsString := Trim(aValue);
    end;
  end;

  function BugReport__.get_ErrorDateAsString :string;
  begin
    if not _ErrorDateSet then
      Result := 'Unknown date'
    else
      Result := _Exe.Error.AsString;
  end;

  procedure BugReport__.set_UserMail (aValue: string);
  begin
    if (__Parens = nil) then begin
      __Parens := PairTokens__.CreateInc(Parentheses);
      __Parens.InsideOnly := False;
      __Parens.NonBlank := true;
      end;
    _UserMail := '';
    __Parens.Line := aValue;
    while __Parens.Next do
       if not __Parens.Inside then begin
         _UserMail := Trim(__Parens.Token);
         Break;
         end;
  end;

  function BugReport__.IsValidProcName (const aProc :string) :boolean;
  var
    New :string;
  begin
    New := Str__.Filtered (aProc, Identifiers + [Dot]);
    if (Length(New) < 3) then
      Result := false
    else if PathStr__.Usable(aProc) <> aProc then
      Result := false
    else
      Result := true
  end;

  function BugReport__.IsValidOffset (const aOffset :string) :boolean;
  begin
    if Length(aOffset) < 2 then
      Result := false
    else if (aOffset[1] <> Plus) then
      Result := false
    else if not NumStr__.IsDigital (Copy(aOffset, 2, 100)) then
      Result := false
    //else if StrToIntDef (aOffset, 1) <= 1 then
    //  Result := false
    else
      Result := true
  end;

  function BugReport__.get_Unique :string;
  var
    i: Integer;
    s: Integer;
  begin
    if _Stack.Count = 0 then
      Result := _Crc
    else begin
      s := 0;
      while (s < _Stack.Count) and (_Stack[s] = _Root) do
        Inc(s);
      if (s >= _Stack.Count) then
        Result := _Crc
      else begin
        Result := _Stack[s];
        for i := Succ(s) to _Stack.TheLast do
          Result := Result + '/' + _Stack[i];
       end;
    end;
  end;

  function BugReport__.GetProcName (const Line :string; out Proc :string) :boolean;
  begin
    _Tokens.Line := Line;
    if not _Tokens.Next then
      Result := false
    else if not IsValidProcName (_Tokens.Token) then
      Result := false
    else begin
      Proc := _Tokens.Token;
      if not _Tokens.Next then
        Result := false
      else if not IsValidOffset (_Tokens.Token) then
        Result := false
      else if not _Tokens.Next then                     //unit offset
        Result := false
      else if not NumStr__.IsDigital (_Tokens.Token) then    //unit offest
        Result := false
      else if not _Tokens.Next then                     //unit
        Result := false
      else if Str__.IsLocated ('Vcl.', _Tokens.Token, 1)  then  //skip system units
        Result := false
      else if Str__.IsLocated ('mad', _Tokens.Token, 1)  then  //skip MadExcept units
        Result := false
      else begin
        while (Proc <> '') and (Proc[1] = Dot) do
          Delete (Proc, 1, 1);
        if (Proc = '') then
          Result := false
        else
          Result := true;
        end;
      end;
  end;

  function BugReport__.GetDateFormat (const Orig :string) :string;
  begin
    Result := Orig;
    System.Delete (Result, Length(Result) - 1, 2); //ms
    System.Delete (Result, 21, 1);                 //", "
    Result[21] := Dot;
  end;

  function BugReport__.Next (out Line: string) :boolean;
  begin
    if (_Index < _Lines.Count) then begin
      Line := _Lines[_Index];
      Inc (_Index);
      Result := True;
      end
    else
      Result := false;
  end;

  function BugReport__.ParseData :boolean;
  var
    line  :string;
    proc  :string;
    Data  :string;

    function Detected (const aTag :string; out aData :string) :boolean;
    begin
      if not Str__.IsLocated (aTag, line, 1) then
        Result := false
      else begin
        aData := System.Copy(Line, 22, 100);
        Result := true;
      end;
    end;

    function ScanFor (const Tag :string) :boolean;
    var
      MainStarted :boolean;
      MainFinished :boolean;
    begin
      Result := false;
      MainStarted := false;
      MainFinished := false;
      _Index := 0;
      while Next (line) and (not Result) and (not MainFinished) do begin
        if MainStarted then begin
          if Str__.IsBlank (line) then
            MainFinished := true
          else if GetProcName (line, proc) then begin
            _Stack.Clear;
            _Stack.Add (proc);
            Title := proc;
            while (_Stack.Count < _Depth) and Next(line) and not Str__.IsBlank(line) do
              if GetProcName (line, Proc) then
                _Stack.Insert (0, Proc);
            Result := true;
            end;
          end
        else if Detected (Tag, Data) then
          MainStarted := true
        else if Detected ('date/time', Data) then
          ErrorDateAsString := GetDateFormat (Data)
        else if Detected ('computer name', Data) then
          _PC.Name := Data
        else if Detected ('user name', Data) then
          _UserName := Data
        else if Detected ('registered owner', Data) then
          _Owner := Data
        else if Detected ('operating system', Data) then
          _PC.OS := Data
        else if Detected ('processors', Data) then
          _PC.Chip := Data
        else if Detected ('physical memory', Data) then
          _PC.Memory := Data
        else if Detected ('executable', Data) then
          _Exe.Name := Data
        else if Detected ('exec. date/time', Data) then
          _Exe.Date.AsString := Data
        else if Detected ('version', Data) then
          _Exe.Version.AsString := Data
        else if Detected ('Lynx user', Data) then
          set_UserMail (Data)
        else if Detected ('exception class', Data) then
          ExcClass := Data
        else if Detected ('exception message', Data) then
          _ExcMessage := Data
        else if Detected ('exception number', Data) then
          _ExcNumber := StrToIntDef(Data, 0)
        else if Detected ('callstack crc', Data) then
          _Crc := Data;
        end;
    end;

  begin
    _Title := '';
    //_Date.Clear;
    _ExcClass := '';
    _ExcMessage := '';
    if ScanFor ('main thread') then
      Result := true
    else if ScanFor ('thread') then
      Result := true
    else if _Crc <> '' then
      Result := True
    else
      Result := false;
    if (_ExcClass = '') then begin
      if Str__.IsIn ('frozen', _ExcMessage) then
        ExcClass := 'Frozen'
      else
        ExcClass := System.Copy (_ExcMessage, 1, 15);
      end;
    if Str__.IsBlank (_Title) then
      Title := ExcClass;
    if not Str__.IsBlank (_Title) then
      Result := true;
  end;

  function BugReport__.get_Title :string;
  begin
    if (_Title = '') then
      Result := 'Unknown title'
    else
      Result := _Title;
  end;

  procedure BugReport__.set_Title (aValue :string);
  var
    i :integer;
  begin
    i := System.Pos (Dollar, aValue);
    if (i = 0) then
      _Title := aValue
    else
      _Title := System.Copy (aValue, 1, Pred(i));
    _Title :=  PathStr__.Usable (_Title);
  end;

  procedure BugReport__.set_ExcClass (const aValue :string);
  begin
    _ExcClass := PathStr__.Usable (aValue);
  end;

  function BugReport__.get_Version :string;
  begin
    Result := _Exe.Version.AsString
  end;

  function BugReport__.get_Executable :string;
  begin               
    Result := _Exe.Name
  end;

  function BugReport__.LoadFromStream (const aValue :TStream) :boolean;
  begin
    aValue.Position := 0;
    _Lines.LoadFromStream (aValue);
    _Index := 0;
    if ParseData then
      _IsValid := true
    else
      _IsValid := false;
    Result := _IsValid;
  end;

  function BugReport__.LoadFromFile(const AName :string) :boolean;
  var
    ms: TMemoryStream;
  begin
    Result := False;
    ms := TMemoryStream.Create;
    try
      ms.LoadFromFile(AName);
      if LoadFromStream(ms) then
        Result := True;
    finally
      ms.Free;
    end;
  end;

  function BugReport__.get_User: string;
  begin
    if Str__.IsBlank (_UserMail) then
      Result := _UserName
    else
      Result := _UserMail
  end;

  function BugReport__.get_Owner :string;
  begin
    if (_Owner = '') then
      Result := '()'
    else
      Result := _Owner;
  end;

  function BugReport__.get_ID: string;
  var
    i :integer;
    ch :Char;
    DateStr :string;
    NewDateStr :string;
    Minutes: integer;
    CodeStr :string;
  begin
    Result := '';
    NewDateStr := '';
    DateStr := ErrorDateAsString;
    Minutes := (Length(DateStr) - 6);
    for i := 1 to Minutes do begin
      ch := DateStr[i];
      if DIG (ch) then
        NewDateStr := NewDateStr + ch
      else if (ch = Space) then
        NewDateStr := NewDateStr + Dot;
      end;
    CodeStr := '';
    for i := Succ(Minutes) to Length(DateStr) do begin
      Ch := DateStr[i];
      if DIG (Ch) then
        CodeStr := CodeStr + Ch;
      end;
    //Code := StrToInt(CodeStr);
    //CodeStr := Base64Str(Code, 5);
    Result :=  Version + '-' + NewDateStr + '.' + CodeStr;
  end;

  function BugReport__.get_Dir: string;
  begin
    Result := Executable + '\' + ExcClass + '\' + Title
  end;

  function BugReport__.Before (Day :DateTime__) :boolean;
  begin
    if (_Exe.Error.Value <= Day.Value) then
      Result := true
    else
      Result := false;
  end;

  function BugReport__.After (Day :DateTime__) :boolean;
  begin
    if (_Exe.Error.Value > Day.Value) then
      Result := true
    else
      Result := false;
  end;

  function BugReport__.get_Prop (AProp  :StrProp_) :string;
  begin
    case AProp of
      Executable_: Result := Executable;
      Version_:    Result := Version;
      ExcClass_:   Result := ExcClass;
      ExcMessage_: Result := ExcMessage;
      ExcOwner_:   Result := Owner;
      Unique_:     Result := Unique;
      PCName_:     Result := PC.Name;
      PCMemory_:   Result := PC.Memory;
      PCChip_:     Result := PC.Chip;
      PCOS_:       Result := PC.OS;
    end;
  end;

initialization
finalization
  FreeAndNil(__Parens);
end.
